package app.secure.com.myapplication.pojo;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;

/**
 * Created by Saimon on 26-Dec-15.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Comment implements Serializable{
    private User user;
    private int commentId;
    private String commentText;
    private boolean isSupported;
    private int countSupport;

    // TODO: remove temporary variables and switch to objects
    private int userId;
    private String username;
    private String userProfileImageUri;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getCommentText() {
        return commentText;
    }

    public void setCommentText(String commentText) {
        this.commentText = commentText;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUserProfileImageUri() {
        return userProfileImageUri;
    }

    public void setUserProfileImageUri(String userProfileImageUri) {
        this.userProfileImageUri = userProfileImageUri;
    }

    public int getCommentId() {
        return commentId;
    }

    public void setCommentId(int commentId) {
        this.commentId = commentId;
    }

    public boolean getIsSupported() {
        return isSupported;
    }

    public void setIsSupported(boolean isSupported) {
        this.isSupported = isSupported;
    }

    public int getCountSupport() {
        return countSupport;
    }

    public void setCountSupport(int countSupport) {
        this.countSupport = countSupport;
    }

    // invert the isSupported boolean and update the support count by +1 or -1,
    // called when user clicks the support button (supports or unsupports a post)
    public void toggleSupport(){
        isSupported = !isSupported;
        countSupport = isSupported ? countSupport+1 : countSupport-1;
    }
}
