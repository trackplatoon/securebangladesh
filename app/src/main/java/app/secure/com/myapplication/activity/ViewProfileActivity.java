package app.secure.com.myapplication.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.facebook.CallbackManager;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.json.JSONObject;

import java.util.ArrayList;

import app.secure.com.myapplication.R;
import app.secure.com.myapplication.adapter.PostAdapter;
import app.secure.com.myapplication.listener.EndlessScrollListener;
import app.secure.com.myapplication.pojo.Post;
import app.secure.com.myapplication.pojo.User;
import app.secure.com.myapplication.pojo.requestprimitive.ResponseObjectFeed;
import app.secure.com.myapplication.pojo.requestprimitive.ResponseObjectViewUser;
import app.secure.com.myapplication.network.NetworkRequestHelper;
import app.secure.com.myapplication.network.TrackSingleton;
import app.secure.com.myapplication.view.CircularNetworkImageView;
import app.secure.com.myapplication.network.FacebookShareHelper;
import app.secure.com.myapplication.network.TrackURL;

public class ViewProfileActivity extends DrawerActivity{

    private int userId;
    private User user;
    private PostAdapter mAdapter;
    private ListView userPostListView;
    private TextView username;
    private int sharedPostId = 0;
    private CircularNetworkImageView profileUserImage;
    private ImageView profileCoverImage;
    private Bitmap bmap;
    private RelativeLayout layoutUserInfo;
    private ArrayList<Post> userPostList;
    private TextView status;
    private Button followButton;
    private LinearLayout mainView;
    private CallbackManager callbackManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_viewprofile);
        // Sets up the drawer and toolbar
        // Call this method after calling setContentView()
        super.init();
        userId = getIntent().getIntExtra("userId", 0);
        username = (TextView) findViewById(R.id.profile_name);
        userPostListView = (ListView) findViewById(R.id.listview_profile);
        profileUserImage = (CircularNetworkImageView) findViewById(R.id.profile_user_image);
        profileCoverImage = (ImageView) findViewById(R.id.profile_cover_image);
        layoutUserInfo = (RelativeLayout) findViewById(R.id.layout_profile_userinfo);
        followButton = (Button) findViewById(R.id.textview_follow_user);
        status = (TextView) findViewById(R.id.textview_viewprofile_status);
        mainView = (LinearLayout) findViewById(R.id.container);

    }

    @Override
    public void onResume(){
        super.onResume();
        NetworkRequestHelper.startActionViewUser(this,
                userId,
                createReqViewUserListener(),
                createReqErrorListener());
    }

    private void customLoadMoreDataFromApi(int page){
        try {
            NetworkRequestHelper.startActionViewPostByUser(ViewProfileActivity.this,
                    userId,
                    page,
                    createReqViewUserPostsListener(),
                    createReqErrorListener());
        }catch (NullPointerException e){e.printStackTrace();}
    };

    // creates and returns a listener that handles the response to view user request
    private Response.Listener<JSONObject> createReqViewUserListener() {
        return new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    // TODO: fix the conversion from JSON to class
                    ObjectMapper mapper = new ObjectMapper();
                    ResponseObjectViewUser responseBody = mapper.readValue(response.toString(), ResponseObjectViewUser.class);
                    user = responseBody.getUser();
                    username.setText(user.getUsername());
                    setFollowButtonState(user.isFollowed());
                    if(user.getUserProfileImageUrl() != null)
                        profileUserImage.setImageUrl(TrackURL.BASE_URL + user.getUserProfileImageUrl(),
                                TrackSingleton.getInstance(ViewProfileActivity.this).getImageLoader());
                    else
                        profileUserImage.setImageBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.avatar_small));
                    status.setVisibility(View.GONE);
                    mainView.setVisibility(View.VISIBLE);
                    // Request the server for user posts
                    userPostListView.setOnScrollListener(new EndlessScrollListener() {
                        @Override
                        public boolean onLoadMore(int page, int totalItemsCount) {
                            // Triggered only when new data needs to be appended to the list
                            // Add whatever code is needed to append new items to your AdapterView
                            customLoadMoreDataFromApi(page);
                            // or customLoadMoreDataFromApi(totalItemsCount);
                            return true; // ONLY if more data is actually being loaded; false otherwise.
                        }
                    });
                    NetworkRequestHelper.startActionViewPostByUser(ViewProfileActivity.this,
                            userId,
                            1,
                            createReqViewUserPostsListener(),
                            createReqErrorListener());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
    }

    // creates and returns a listener that handles the response to view user posts request
    private Response.Listener<JSONObject> createReqViewUserPostsListener() {
        return new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    // TODO: fix the conversion from JSON to class
                    ObjectMapper mapper = new ObjectMapper();
                    ResponseObjectFeed responseBody = mapper.readValue(response.toString(), ResponseObjectFeed.class);
                    userPostList = (ArrayList) responseBody.getPosts();
                    mAdapter = new PostAdapter(ViewProfileActivity.this, userPostList);
                    userPostListView.setAdapter(mAdapter);
                    followButton.setVisibility(View.VISIBLE);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
    }

    private Response.ErrorListener createReqErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        };
    }

    // handle onClick event on username, user_thumbnail image
    public void usernameClick(View v) {
        // do nothing, we're already on the view user activity
    }

    public void followUser(View v){
        if(userId != 0) {
            user.toggleIsFollowed();
            NetworkRequestHelper.startActionFollowUser(this, userId, user.isFollowed());
            setFollowButtonState(user.isFollowed());
        }
    }

    public void supportPostClick(View v) {
        int position = (int) v.getTag();
        try {
            userPostList.get(position).toggleSupport();
            mAdapter.notifyDataSetChanged();
            NetworkRequestHelper.startActionSupportPost(this,
                    userPostList.get(position).getPostId(),
                    userPostList.get(position).isSupported());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void commentClick(View v) {
        int position = (int) v.getTag();
        Intent intent = new Intent(this, ViewPostActivity.class);
        intent.putExtra("POST_ID", userPostList.get(position).getPostId());
        intent.putExtra("POST", userPostList.get(position));
        startActivity(intent);
    }

    public void sharePostClick(View v) {
        if(callbackManager == null)
            callbackManager = FacebookShareHelper.init(this);
        Post post = userPostList.get((int) v.getTag());
        FacebookShareHelper.share(this,
                post,
                "http://www.trackplatoon.com");
    }

    private void setFollowButtonState(boolean isFollowed){
        if(isFollowed){
            followButton.setText("You are following this person");
            followButton.setBackground(getResources().getDrawable(R.color.color_post_button));
        }else{
            followButton.setText("Click to follow this person");
            followButton.setBackground(getResources().getDrawable(R.color.color_dark_grey));
        }
    }

    // facebook share confirmation
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.e("ViewProfileActivity", "Calling onActivityResult");
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
        if(resultCode == Activity.RESULT_OK){
            Log.e("RESULT CODEs", Integer.toString(resultCode));
            //SEND THE SHARE POST REQUEST
        }
        else {
            Log.e("RESULT ", Integer.toString(resultCode));
        }

    }

}
