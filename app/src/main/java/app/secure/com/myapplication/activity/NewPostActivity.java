package app.secure.com.myapplication.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.soundcloud.android.crop.Crop;

import org.json.JSONObject;

import java.io.File;

import app.secure.com.myapplication.R;
import app.secure.com.myapplication.pojo.Post;
import app.secure.com.myapplication.pojo.requestprimitive.ResponseObjectSuccess;
import app.secure.com.myapplication.network.NetworkRequestHelper;
import app.secure.com.myapplication.view.BitmapHelper;

public class NewPostActivity extends AppCompatActivity{

    private Toolbar toolbar;
    private Button btnPost;
    private ImageButton btnAddImage;
    private Spinner spinnerPostType;
    private Spinner spinnerReportType;
    private EditText etPostText;
    private CheckBox chkboxAnonymous;
    private ImageView postImagePrimary;
    private LinearLayout layoutReportType;
    private ProgressDialog progress;

    private Bitmap postImageBitmap;

    private int PICK_IMAGE_REQUEST = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_newpost);
        toolbar = (Toolbar) findViewById(R.id.toolbar_post);
        spinnerPostType = (Spinner) findViewById(R.id.spinner_post_type);
        spinnerReportType = (Spinner) findViewById(R.id.spinner_report_type);
        etPostText = (EditText) findViewById(R.id.edittext_post);
        layoutReportType = (LinearLayout) findViewById(R.id.layout_report_type);
        postImagePrimary = (ImageView) findViewById(R.id.post_image_1);
        chkboxAnonymous = (CheckBox) findViewById(R.id.checkbox_anonymous);
        btnPost = (Button) findViewById(R.id.button_post);
        btnAddImage = (ImageButton) findViewById(R.id.button_post_camera);
        if (toolbar != null) {
            toolbar.inflateMenu(R.menu.menu_post);
        }
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle("New Post");
        spinnerPostType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                switch (position) {
                    case 0:
                        layoutReportType.setVisibility(View.VISIBLE);
                        break;
                    default:
                        layoutReportType.setVisibility(View.GONE);
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
            }
        });

        btnAddImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showFileChooser();
            }
        });

        // collect data from UI and send to intentService
        btnPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // allow posting only if the string is not entirely composed of whitespaces
                if (etPostText.getText().toString().replaceAll("\\s+", "") != "") {
                    progress = ProgressDialog.show(NewPostActivity.this, "Posting report",
                            "please wait...", true);
                    v.setEnabled(false);
                    Post post = new Post();
                    post.setPostText(etPostText.getText().toString().trim()); // remove leading and trailing spaces
                    switch (spinnerPostType.getSelectedItemPosition()) {
                        case 1:
                            post.setReportType(1);
                            break;
                        case 2:
                            post.setReportType(2);
                            break;
                        case 0:
                            post.setReportType(51 + spinnerReportType.getSelectedItemPosition());
                            break;
                    }
                    post.setIsAnonymous(chkboxAnonymous.isChecked());
                    Log.e("DEBUG", "Am I anonymous? " + post.getIsAnonymous());
                    post.setPostImagePrimary(BitmapHelper.getStringImage(postImageBitmap));
                    Log.e("DEBUG", "Image: " + post.getPostImagePrimary());
                    NetworkRequestHelper.startActionNewPost(NewPostActivity.this,
                            post,
                            createNewPostReqListener(),
                            createReqErrorListener());
                }
            }
        });
    }

    @Override
    protected void onResume(){
        super.onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (toolbar == null || true) {
            toolbar.inflateMenu(R.menu.menu_post);
        }
        return super.onCreateOptionsMenu(menu);
    }

    public void toggleAnonymous(View view){
        chkboxAnonymous.setChecked(!chkboxAnonymous.isChecked());
    }

    // asks the user to pick an application for choosing an image
    public void showFileChooser() {
        Log.e("PICK PHOTO", "Method on fragment called");
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent result) {
        Log.e("OnActivity result", "requestCode: " + requestCode);
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == Activity.RESULT_OK && result != null && result.getData() != null) {
            Log.e("CROP", "Calling beginCrop");
            beginCrop(result.getData());
        } else if (requestCode == Crop.REQUEST_CROP) {
            Log.e("CROP", "Calling handleCrop");
            handleCrop(resultCode, result);
        }
    }

    public void beginCrop(Uri source) {
        Log.e("CROP", "Executing beginCrop");
        Uri destination = Uri.fromFile(new File(getCacheDir(), "cropped"));
        Crop.of(source, destination).asSquare().start(this);
    }

    public void handleCrop(int resultCode, Intent result) {
        Log.e("CROP", "Executing handleCrop with resultCode: " + resultCode);
        if (resultCode == Activity.RESULT_OK) {
            //profileImageView.setImageURI(Crop.getOutput(result))
            Uri filePath = Crop.getOutput(result);
            Log.e("CROP", "Uri filepath = " + filePath);
            postImageBitmap = BitmapHelper.getScaledBitmap(this, filePath, 600, 600);
            if(postImageBitmap == null)
                Log.e("CROP", "Null image");
            Bitmap bmap = BitmapFactory.decodeResource(getResources(), R.drawable.avatar_small);
            postImagePrimary.setImageBitmap(postImageBitmap);
        } else if (resultCode == Crop.RESULT_ERROR) {
            Toast.makeText(this, Crop.getError(result).getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == android.R.id.home) {
            // Do nothing, android goes back to parent activity stated in AndroidManifest.xml
        } else if (id == R.id.action_save_post) {
            Toast.makeText(getApplicationContext(), "Post saved",
                    Toast.LENGTH_SHORT).show();
        }
        return super.onOptionsItemSelected(item);
    }

    // creates and returns a listener that handles the response to new post request
    private Response.Listener<JSONObject> createNewPostReqListener() {
        return new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                progress.dismiss();
                try {
                    // TODO: fix the conversion from JSON to class
                    ObjectMapper mapper = new ObjectMapper();
                    ResponseObjectSuccess responseBody = mapper.readValue(response.toString(),
                            ResponseObjectSuccess.class);
                    if(responseBody.isSuccess())
                        Toast.makeText(NewPostActivity.this, "Posted!", Toast.LENGTH_SHORT).show();
                    else
                        Toast.makeText(NewPostActivity.this, "Error: invalid server response", Toast.LENGTH_SHORT).show();
                    finish();
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(NewPostActivity.this, "Error: invalid server response", Toast.LENGTH_SHORT).show();
                }
            }
        };
    }

    private Response.ErrorListener createReqErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progress.dismiss();
                Toast.makeText(NewPostActivity.this, "Error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
                btnPost.setEnabled(true);
            }
        };
    }
}
