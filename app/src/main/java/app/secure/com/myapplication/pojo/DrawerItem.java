package app.secure.com.myapplication.pojo;

/**
 * Created by Saimon on 31-Oct-15.
 */
public class DrawerItem {
    private String name;
    private int iconID;
    private String userProfileImageUrl;
    private int notificationCount;

    public DrawerItem(String name, int iconResID, int notificationCount) {
        this.name = name;
        this.iconID = iconResID;
        this.notificationCount = notificationCount;
    }

    public DrawerItem(String name, int iconResID) {
        this.name = name;
        this.iconID = iconResID;
        this.notificationCount = 0;
    }

    public DrawerItem(String profileImageID) {
        this.userProfileImageUrl = profileImageID;
        this.iconID = 0;
        this.name = "";
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIconResID() {
        return iconID;
    }

    public void setIconResID(int iconId) {
        this.iconID = iconId;
    }

    public String getUserProfileImageUrl() {
        return userProfileImageUrl;
    }

    public void setUserProfileImageUrl(String userProfileImageUrl) {
        this.userProfileImageUrl = userProfileImageUrl;
    }

    public int getNotificationCount() {
        return notificationCount;
    }

    public void setNotificationCount(int notificationCount) {
        this.notificationCount = notificationCount;
    }
}
