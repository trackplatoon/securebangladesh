package app.secure.com.myapplication.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import app.secure.com.myapplication.R;
import app.secure.com.myapplication.pojo.Notification;

/**
 * Created by Arafat on 03/12/2015.
 */
public class NotificationAdapater extends BaseAdapter {
    private ArrayList<Notification> notificationList;
    private Context context;
    private LayoutInflater inflater = null;

    public NotificationAdapater(Context context, ArrayList<Notification> notificationList) {
        this.context = context;
        this.notificationList = notificationList;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return notificationList.size();
    }

    @Override
    public Object getItem(int position) {
        return notificationList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        Holder holder;
        if (view == null) {
            holder = new Holder();
            view = inflater.inflate(R.layout.list_item_notification, null);
            holder.container = (LinearLayout) view.findViewById(R.id.layout_notification_item);
            holder.notificationText = (TextView) view.findViewById(R.id.notifier_name);
            holder.icon = (ImageView) view.findViewById(R.id.notification_icon);
            view.setTag(holder);
        } else {
            holder = (Holder) view.getTag();
        }
        try {
            if ( !notificationList.get(position).getIsRead() )
                holder.container.setBackgroundColor(context.getResources().getColor(R.color.color_notification_unread));
            else
                holder.container.setBackgroundColor(context.getResources().getColor(R.color.color_white));
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        holder.notificationText.setText(notificationList.get(position).getNotificationText());
        switch (notificationList.get(position).getType()){
            case "support":
                holder.icon.setImageDrawable(context.getResources().getDrawable(R.drawable.support_ico_press));
                break;
            case "follow":
                holder.icon.setImageDrawable(context.getResources().getDrawable(R.drawable.friend_ico_notificatoin));
                break;
            case "share":
                holder.icon.setImageDrawable(context.getResources().getDrawable(R.drawable.share_ico_notificatoin));
                break;
            case "comment":
                holder.icon.setImageDrawable(context.getResources().getDrawable(R.drawable.comment_ico_notificatoin));
                break;
        }
        return view;
    }

    public class Holder {
        LinearLayout container;
        TextView notificationText;
        ImageView icon;
    }
}
