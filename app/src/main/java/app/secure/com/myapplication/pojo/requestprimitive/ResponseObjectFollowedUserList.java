package app.secure.com.myapplication.pojo.requestprimitive;

import java.io.Serializable;
import java.util.List;

import app.secure.com.myapplication.pojo.User;

/**
 * Created by Saimon on 17-Jan-16.
 */
public class ResponseObjectFollowedUserList implements Serializable{
    private boolean success;
    private int countFollowing;
    private List<User> users;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public int getCountFollowing() {
        return countFollowing;
    }

    public void setCountFollowing(int countFollowing) {
        this.countFollowing = countFollowing;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }
}
