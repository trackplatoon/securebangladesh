package app.secure.com.myapplication.view;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.util.Base64;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;

/**
 * Created by Saimon on 08-Mar-16.
 */
public class BitmapHelper {

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }


    public static Bitmap getScaledBitmap(Context context, Uri filePath, int width, int height){
        Bitmap bitmap;
        try {
            AssetFileDescriptor fileDescriptor =
                    context.getContentResolver().openAssetFileDescriptor(filePath, "r");
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFileDescriptor(fileDescriptor.getFileDescriptor(), null, options);
            options.inSampleSize = BitmapHelper.calculateInSampleSize(options, width, height);
            // Decode bitmap with inSampleSize set
            options.inJustDecodeBounds = false;
            bitmap =
                    BitmapFactory.decodeFileDescriptor(fileDescriptor.getFileDescriptor(), null, options);
        }catch (FileNotFoundException e){
            e.printStackTrace();
            return null;
        }
        return bitmap;
    }

    // converts Bitmap image to Base64 encoded image
    public static String getStringImage(Bitmap bmp) {
        if(bmp == null)
            return null;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }
}
