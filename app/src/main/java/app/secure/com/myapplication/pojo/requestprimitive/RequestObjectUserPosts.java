package app.secure.com.myapplication.pojo.requestprimitive;

/**
 * Created by Saimon on 15-Jan-16.
 */
public class RequestObjectUserPosts {
    private int userId;

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
}
