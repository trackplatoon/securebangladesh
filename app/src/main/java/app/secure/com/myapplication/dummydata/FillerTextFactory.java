package app.secure.com.myapplication.dummydata;

import java.util.Random;

/**
 * Created by Saimon on 24-Oct-15.
 */
public class FillerTextFactory {
    public final static int minStringLength = 10;

    public final static String baseText = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. " +
            "Quisque mollis eros urna, a fringilla tellus facilisis non. Sed sed consectetur diam, sed " +
            "interdum nibh. Nulla sit amet rutrum sapien. Ut sagittis congue dui eu bibendum. Nullam bibendum," +
            " libero et fringilla mollis, enim mauris aliquet turpis, ac molestie tortor turpis quis nunc. " +
            "Donec interdum, mi non tempor interdum, erat arcu posuere orci, a efficitur arcu nunc id arcu. Nunc" +
            " efficitur sed mi ut euismod. In varius lectus quis vestibulum pellentesque. Etiam efficitur, eros" +
            " vitae convallis elementum, lectus orci viverra est, vitae fermentum magna urna in massa. Maecenas" +
            " faucibus molestie gravida. Vivamus semper odio ut sapien pellentesque vestibulum. Nulla eu varius " +
            "arcu. Nullam placerat sollicitudin arcu, vel rutrum dolor pharetra non.\n" +
            "\n" +
            "Nunc pulvinar, mauris et auctor aliquet, leo eros pulvinar turpis, in rhoncus turpis odio non ex. " +
            "Curabitur finibus pellentesque varius. Nullam accumsan faucibus lacinia. Vivamus euismod ullamcorper" +
            " mi sed placerat. Morbi a tellus interdum, ultrices diam quis, cursus sapien. Integer mollis tincidunt" +
            " nisi iaculis venenatis. Nulla feugiat non orci ut venenatis. Maecenas eget turpis sit amet nisl sollicitudin" +
            " ornare bibendum et nibh. Aliquam quis dapibus risus. Suspendisse ullamcorper malesuada nibh, vel auctor velit" +
            " auctor eget. Nullam eu turpis sed ligula pretium.";

    public static String getString() {
        return baseText.substring(0, new Random().nextInt(baseText.length() - minStringLength) + minStringLength);
    }

    public static String getString(int length) {
        if (length > baseText.length()) length = baseText.length();
        return baseText.substring(0, length);
    }
}
