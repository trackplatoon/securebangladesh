package app.secure.com.myapplication.pojo.requestprimitive;

/**
 * Created by Saimon on 17-Jan-16.
 */
public class RequestObjectFollowUser {
    private int userId;
    // TODO: change to boolean (fix server issue)
    private String isFollowed;

    public RequestObjectFollowUser(int userId, boolean isFollowed){
        setUserId(userId);
        setIsFollowed(isFollowed);
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getIsFollowed() {
        return isFollowed;
    }

    public void setIsFollowed(boolean isFollowed) {
        this.isFollowed = isFollowed ? "true" : "false";
    }
}
