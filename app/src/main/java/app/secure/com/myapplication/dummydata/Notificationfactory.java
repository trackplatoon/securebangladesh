package app.secure.com.myapplication.dummydata;

import java.util.ArrayList;

import app.secure.com.myapplication.pojo.Friend;
import app.secure.com.myapplication.pojo.Notification;

/**
 * Created by Arafat on 03/12/2015.
 */
public class Notificationfactory {

    public static ArrayList<Notification> getDummyNotifications(){
        ArrayList<Notification> notificationList = new ArrayList<>();
        Notification notification;

        //1
        notification = new Notification();
        notification.setNotificationText("John Doe supported your post");
        notificationList.add(notification);

        //2
        notification = new Notification();
        notification.setNotificationText("Albert Mosses shared your post");
        notificationList.add(notification);


        //3
        notification = new Notification();
        notification.setNotificationText("Royal Albert commented on your post");
        notificationList.add(notification);


        //4
        notification = new Notification();
        notification.setNotificationText("Azis Supported your post");
        notificationList.add(notification);

        //5
        notification = new Notification();
        notification.setNotificationText("Mufazzal shared Your post");
        notificationList.add(notification);


        //6
        notification = new Notification();
        notification.setNotificationText("Gazi Shuvo supported your post");
        notificationList.add(notification);


        //7
        notification = new Notification();
        notification.setNotificationText("Soniya commented on your post");
        notificationList.add(notification);


        //8
        notification = new Notification();
        notification.setNotificationText("John supported your post");
        notificationList.add(notification);


        //9
        notification = new Notification();
        notification.setNotificationText("Saiful Saif commented on your post");
        notificationList.add(notification);


        //10
        notification = new Notification();
        notification.setNotificationText("Lori Williams supported your post");
        notificationList.add(notification);

        return notificationList;
    }

}
