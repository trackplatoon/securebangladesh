package app.secure.com.myapplication.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import app.secure.com.myapplication.R;
import app.secure.com.myapplication.global.Credentials;
import app.secure.com.myapplication.global.TrackPlatoon;

public class StartActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String accessToken = (((TrackPlatoon) getApplication()).getCredentials()).getToken();
        // if user is logged in, go to HomeActivity. Else go to StartActivity.
        if(accessToken != null)
            startActivity(new Intent(StartActivity.this, HomeActivity.class));
        else
            startActivity(new Intent(StartActivity.this, SignInActivityNew.class));
        finish();
    }

}
