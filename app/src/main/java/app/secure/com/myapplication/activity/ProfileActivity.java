package app.secure.com.myapplication.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.facebook.CallbackManager;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.json.JSONObject;

import java.util.ArrayList;

import app.secure.com.myapplication.R;
import app.secure.com.myapplication.adapter.PostAdapter;
import app.secure.com.myapplication.global.TrackPlatoon;
import app.secure.com.myapplication.listener.EndlessScrollListener;
import app.secure.com.myapplication.pojo.Post;
import app.secure.com.myapplication.pojo.User;
import app.secure.com.myapplication.pojo.requestprimitive.ResponseObjectFeed;
import app.secure.com.myapplication.pojo.requestprimitive.ResponseObjectViewUser;
import app.secure.com.myapplication.network.NetworkRequestHelper;
import app.secure.com.myapplication.network.TrackSingleton;
import app.secure.com.myapplication.view.CircularNetworkImageView;
import app.secure.com.myapplication.network.FacebookShareHelper;
import app.secure.com.myapplication.network.TrackURL;

public class ProfileActivity extends DrawerActivity{
    // TODO: load from memory/server
    private static final int USER_IMAGE = R.drawable.avatar;
    private static final int COVER_IMAGE = R.drawable.cover_min;

    private User user;
    private ArrayList<Post> userPostList;
    private PostAdapter mAdapter;
    private ListView userPostListView;
    private TextView username;
    private CircularNetworkImageView profileUserImage;
    private ImageView profileCoverImage;
    private ImageButton buttonAbout;
    private ImageButton buttonSavedPost;
    private ImageButton buttonFriends;
    private RelativeLayout layoutUserInfo;
    private TextView status;
    private LinearLayout mainView;
    private CallbackManager callbackManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        // initialize drawer and toolbar after calling setContentView
        super.init();
        username = (TextView) findViewById(R.id.username);
        userPostListView = (ListView) findViewById(R.id.listview_profile);
        buttonSavedPost = (ImageButton) findViewById(R.id.profile_menu_saved_post);
        buttonFriends = (ImageButton) findViewById(R.id.profile_menu_friends);
        buttonAbout = (ImageButton) findViewById(R.id.profile_menu_about);
        profileUserImage = (CircularNetworkImageView) findViewById(R.id.profile_user_image);
        profileCoverImage = (ImageView) findViewById(R.id.profile_cover_image);
        layoutUserInfo = (RelativeLayout) findViewById(R.id.layout_profile_userinfo);
        status = (TextView) findViewById(R.id.textview_viewprofile_status);
        mainView = (LinearLayout) findViewById(R.id.container);

        // TODO: implement saved post list
        buttonSavedPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(ProfileActivity.this, "Not supported yet!", Toast.LENGTH_SHORT).show();
            }
        });

        // Launch Friend List Activity
        buttonFriends.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ProfileActivity.this, FollowedListActivity.class);
                startActivity(intent);
            }
        });

        // Launch Home Activity
        buttonAbout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ProfileActivity.this, AboutActivity.class);
                intent.putExtra("VIEW_ONLY", true);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            }
        });
    }

    @Override
    public void onResume(){
        super.onResume();
        // call the service for data
        //TrackReceiver mReceiver = new TrackReceiver(new Handler());
        //mReceiver.setListener(this);
        Log.e("View User", "Started ProfileActivity with userId = "
                + ((TrackPlatoon)getApplication()).getCredentials().getUserId());
        NetworkRequestHelper.startActionViewUser(this,
                ((TrackPlatoon)getApplication()).getCredentials().getUserId(),
                createReqViewUserListener(),
                createReqErrorListener());
    }

    // handle onClick event on username, user_thumbnail image
    public void usernameClick(View v) {
        // do nothing, we're already on the view user activity
    }

    private void customLoadMoreDataFromApi(int page){
        try {
            NetworkRequestHelper.startActionViewPostByUser(ProfileActivity.this,
                    user.getUserId(),
                    page,
                    createReqViewUserPostsListener(),
                    createReqErrorListener());
        }catch (NullPointerException e){e.printStackTrace();}
    };

    // creates and returns a listener that handles the response to view user request
    private Response.Listener<JSONObject> createReqViewUserListener() {
        return new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    Log.e("Volley Response", response.toString());
                    // TODO: fix the conversion from JSON to class
                    ObjectMapper mapper = new ObjectMapper();
                    ResponseObjectViewUser responseBody = mapper.readValue(response.toString(), ResponseObjectViewUser.class);
                    user = responseBody.getUser();
                    username.setText(user.getUsername());
                    if(user.getUserProfileImageUrl()  != null)
                        profileUserImage.setImageUrl(TrackURL.BASE_URL + user.getUserProfileImageUrl() ,
                                TrackSingleton.getInstance(ProfileActivity.this).getImageLoader());
                    else
                        profileUserImage.setImageBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.avatar_small));
                    status.setVisibility(View.GONE);
                    mainView.setVisibility(View.VISIBLE);
                    // Request the server for user posts
                    // Request the server for user posts
                    userPostListView.setOnScrollListener(new EndlessScrollListener() {
                        @Override
                        public boolean onLoadMore(int page, int totalItemsCount) {
                            // Triggered only when new data needs to be appended to the list
                            // Add whatever code is needed to append new items to your AdapterView
                            customLoadMoreDataFromApi(page);
                            // or customLoadMoreDataFromApi(totalItemsCount);
                            return true; // ONLY if more data is actually being loaded; false otherwise.
                        }
                    });
                    NetworkRequestHelper.startActionViewPostByUser(ProfileActivity.this,
                            user.getUserId(),
                            1,
                            createReqViewUserPostsListener(),
                            createReqErrorListener());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
    }

    // creates and returns a listener that handles the response to view user posts request
    private Response.Listener<JSONObject> createReqViewUserPostsListener() {
        return new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    Log.e("Volley Response", response.toString());
                    // TODO: fix the conversion from JSON to class
                    ObjectMapper mapper = new ObjectMapper();
                    ResponseObjectFeed responseBody = mapper.readValue(response.toString(), ResponseObjectFeed.class);
                    userPostList = (ArrayList) responseBody.getPosts();
                    mAdapter = new PostAdapter(ProfileActivity.this, userPostList);
                    userPostListView.setAdapter(mAdapter);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
    }

    private Response.ErrorListener createReqErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Volley Response", error.toString());
            }
        };
    }

        public void supportPostClick(View v) {
        int position = (int) v.getTag();
        try {
            userPostList.get(position).toggleSupport();
            mAdapter.notifyDataSetChanged();
            NetworkRequestHelper.startActionSupportPost(this,
                    userPostList.get(position).getPostId(),
                    userPostList.get(position).isSupported());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void commentClick(View v) {
        int position = (int) v.getTag();
        Intent intent = new Intent(this, ViewPostActivity.class);
        Log.e("commentClick", "3: initiated intent");
        intent.putExtra("POST_ID", userPostList.get(position).getPostId());
        intent.putExtra("POST", userPostList.get(position));
        startActivity(intent);
    }

    public void sharePostClick(View v) {
        Log.e("sharePostClick", "1. Method entry point");
        if(callbackManager == null)
            callbackManager = FacebookShareHelper.init(this);
        Post post = userPostList.get((int) v.getTag());
        FacebookShareHelper.share(this,
                post,
                "http://www.trackplatoon.com");
    }

    // facebook share confirmation
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.e("ViewProfileActivity", "Calling onActivityResult");
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
        if(resultCode == Activity.RESULT_OK){
            Log.e("RESULT CODEs", Integer.toString(resultCode));
            //SEND THE SHARE POST REQUEST
        }
        else {
            Log.e("RESULT ", Integer.toString(resultCode));
        }

    }

}
