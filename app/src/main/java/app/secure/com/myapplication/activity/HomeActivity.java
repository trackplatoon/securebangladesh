package app.secure.com.myapplication.activity;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;

import com.facebook.CallbackManager;

import app.secure.com.myapplication.R;
import app.secure.com.myapplication.adapter.TabsPagerAdapter;
import app.secure.com.myapplication.customtoolbar.SplitToolbar;
import app.secure.com.myapplication.fragment.DiscoverPeopleFragment;
import app.secure.com.myapplication.fragment.NewsfeedFragment;
import app.secure.com.myapplication.global.TrackPlatoon;
import app.secure.com.myapplication.network.FacebookShareHelper;

/**
 * Created by Saimon on 16-Oct-15.
 */
public class HomeActivity extends DrawerActivity{

    private LinearLayout fragmentContainer;
    private ActionBar actionBar;
    private SplitToolbar toolbar;
    private TabsPagerAdapter mAdapter;
    private OnClickListener tabSelectListener;
    private ViewPager mPager;
    private Button tabHeaderNewsfeed;
    private Button tabHeaderAroundMe;
    private DiscoverPeopleFragment discoverPeopleFragment;
    private NewsfeedFragment newsfeedFragment;
    private CallbackManager callbackManager;


    FragmentTransaction transaction;
    // Tab titles
    private String[] tabs = {"News Feed", "Around Me"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        callbackManager = FacebookShareHelper.init(this);
        super.init();

        tabHeaderNewsfeed = (Button) findViewById(R.id.tabheader_newsfeed);
        tabHeaderAroundMe = (Button) findViewById(R.id.tabheader_aroundme);

        newsfeedFragment = new NewsfeedFragment();
        discoverPeopleFragment = new DiscoverPeopleFragment();

        mAdapter = new TabsPagerAdapter(getSupportFragmentManager(), newsfeedFragment, discoverPeopleFragment);
        mPager = (ViewPager) findViewById(R.id.pager);
        mPager.setAdapter(mAdapter);
        mPager.setCurrentItem(0);

        mPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                switchTab(position);
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
            }

            @Override
            public void onPageScrollStateChanged(int arg0) {
            }
        });

        tabSelectListener = new OnClickListener() {
            @Override
            public void onClick(View v) {
                // change the current fragment on the pager based on tab click
                switch (v.getId()) {
                    case R.id.tabheader_newsfeed:
                        mPager.setCurrentItem(0);
                        break;
                    case R.id.tabheader_aroundme:
                        mPager.setCurrentItem(1);
                        break;
                    default:
                        mPager.setCurrentItem(0);
                        break;
                }
            }
        };
        tabHeaderNewsfeed.setOnClickListener(tabSelectListener);
        tabHeaderAroundMe.setOnClickListener(tabSelectListener);
    }

    // load data from server on activity resume
    @Override
    protected void onResume(){
        super.onResume();
    }

    /**
     * make tab selected
     */
    public void switchTab(int id) {
        tabHeaderNewsfeed.setBackground(getResources().getDrawable(R.color.tabheader_deselected));
        tabHeaderAroundMe.setBackground(getResources().getDrawable(R.color.tabheader_deselected));
        switch (id) {
            case 0:
                tabHeaderNewsfeed.setBackground(getResources().getDrawable(R.drawable.bg_tabheader_selected));
                break;
            case 1:
                tabHeaderAroundMe.setBackground(getResources().getDrawable(R.drawable.bg_tabheader_selected));
                break;
        }
    }



    // facebook share confirmation
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.e("HomeActivity", "Calling onActivityResult");
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
        if(resultCode == Activity.RESULT_OK){
            Log.e("RESULT CODEs",Integer.toString(resultCode));
        }
        else {
            Log.e("RESULT ",Integer.toString(resultCode));
        }

    }

    // onClick methods
    public void supportPostClick(View v){
        newsfeedFragment.handleSupportPostClick(v);
    }

    public void commentClick(View v){
        newsfeedFragment.handleCommentClick(v);
    }

    public void sharePostClick(View v){
        newsfeedFragment.sharePostClick(v);
    }

    public void usernameClick(View v){
        Log.e("View user", "Call to usernameClick()");
        int userId = 0;
        try {
            userId = (Integer) v.getTag();
        }catch (NullPointerException e){ Log.e("OOPS", "Null username"); return; }
        Log.e("View user", "userId = " + userId);
        if (userId != 0) {
            // go to my own profile
            if(userId == ((TrackPlatoon) getApplication()).getCredentials().getUserId()) {
                Log.e("ERROR", "Going to own profile, userId: " + userId);
                startActivity(new Intent(HomeActivity.this, ProfileActivity.class));
            }
            // go to another user profile other than my own
            else {
                Log.e("ERROR", "Going to some other profile, userId: " + userId);
                Intent intent = new Intent(this, ViewProfileActivity.class);
                intent.putExtra("userId", userId);
                startActivity(intent);
            }
        }
    }
}
