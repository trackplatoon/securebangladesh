package app.secure.com.myapplication.fragment;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.soundcloud.android.crop.Crop;


import java.io.File;

import app.secure.com.myapplication.R;
import app.secure.com.myapplication.pojo.requestprimitive.RequestObjectSignUp;
import app.secure.com.myapplication.network.TrackSingleton;
import app.secure.com.myapplication.view.BitmapHelper;
import app.secure.com.myapplication.view.CircularNetworkImageView;
import app.secure.com.myapplication.network.TrackURL;

public class EditProfileFragment extends Fragment {

    private static final String ARG_PARAM1 = "param1";
    private int PICK_IMAGE_REQUEST = 1;

    //fields
    private EditText etAddress;
    private EditText etStreetLine1;
    private EditText etStreetLine2;
    private EditText etCity;
    private EditText etZip;
    private EditText etRegion;
    // TODO: change to spinner
    private EditText etCountry;
    // TODO: change to datepicker
    private EditText etBirthday;
    private Spinner spSex;
    private EditText etReligion;
    private EditText etContact;
    private EditText etHighschool;
    private EditText etCollege;
    private EditText etUniversity;
    private EditText etProfession;
    private CircularNetworkImageView profileImageView;
    private ImageView coverImageView;

    //bitmaps
    private Bitmap profileImageBitmap;
    private Bitmap coverImageBitmap;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     */
    public static EditProfileFragment newInstance() {
        EditProfileFragment fragment = new EditProfileFragment();
        return fragment;
    }

    public EditProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_edit_profile, container, false);
        etAddress = (EditText) rootView.findViewById(R.id.edittext_address);
        etStreetLine1 = (EditText) rootView.findViewById(R.id.edittext_street_line_1);
        etStreetLine2 = (EditText) rootView.findViewById(R.id.edittext_street_line_2);
        etCity = (EditText) rootView.findViewById(R.id.edittext_city);
        etZip = (EditText) rootView.findViewById(R.id.edittext_zip);
        etRegion = (EditText) rootView.findViewById(R.id.edittext_region);
        etCountry = (EditText) rootView.findViewById(R.id.edittext_country);
        etBirthday = (EditText) rootView.findViewById(R.id.edittext_birthday);
        spSex = (Spinner) rootView.findViewById(R.id.spinner_sex);
        etReligion = (EditText) rootView.findViewById(R.id.edittext_religion);
        etContact = (EditText) rootView.findViewById(R.id.edittext_contact);
        etHighschool = (EditText) rootView.findViewById(R.id.edittext_highschool);
        etCollege = (EditText) rootView.findViewById(R.id.edittext_college);
        etUniversity = (EditText) rootView.findViewById(R.id.edittext_university);
        etProfession = (EditText) rootView.findViewById(R.id.edittext_profession);
        profileImageView = (CircularNetworkImageView) rootView.findViewById(R.id.img_view_pro_pic);
        coverImageView = (ImageView) rootView.findViewById(R.id.img_view_cover_pic);
        profileImageView.setImageBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.avatar_small));
        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    // asks the user to pick an application for choosing an image
    public void showFileChooser() {
        Log.e("PICK PHOTO", "Method on fragment called");
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }

    public void beginCrop(Uri source) {
        Log.e("CROP", "Executing beginCrop");
        Uri destination = Uri.fromFile(new File(getActivity().getCacheDir(), "cropped"));
        Crop.of(source, destination).asSquare().start(getActivity());
    }

    public void handleCrop(int resultCode, Intent result) {
        Log.e("CROP", "Executing handleCrop with resultCode: " +resultCode);
        if (resultCode == Activity.RESULT_OK) {
            //profileImageView.setImageURI(Crop.getOutput(result))
         Uri filePath = Crop.getOutput(result);
            Log.e("CROP", "Uri filepath = " + filePath);
            profileImageBitmap = BitmapHelper.getScaledBitmap(getActivity(), filePath, 300, 300);
            if(profileImageBitmap == null)
                Log.e("CROP", "Null image");
            Bitmap bmap = BitmapFactory.decodeResource(getActivity().getResources(), R.drawable.avatar_small);
            profileImageView.setImageBitmap(profileImageBitmap);
        } else if (resultCode == Crop.RESULT_ERROR) {
            Toast.makeText(getActivity(), Crop.getError(result).getMessage(), Toast.LENGTH_SHORT).show();
        }
    }


//    // process the bitmap and set it to imageview
//    @Override
//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        if (requestCode == PICK_IMAGE_REQUEST && resultCode == Activity.RESULT_OK && data != null && data.getData() != null) {
//            Uri filePath = data.getData();
//            profileImageBitmap = BitmapHelper.getScaledBitmap(getActivity(), filePath, 100, 100);
//            profileImageView.setImageBitmap(profileImageBitmap);
//        }
//    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent result) {
        Log.e("OnActivity result", "requestCode: " + requestCode);
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == Activity.RESULT_OK && result != null && result.getData() != null) {
            Log.e("CROP", "Calling beginCrop");
            beginCrop(result.getData());
        } else if (requestCode == Crop.REQUEST_CROP) {
            Log.e("CROP", "Calling handleCrop");
            handleCrop(resultCode, result);
        }
    }


    // packs all the edittext data into an object
    public RequestObjectSignUp collectData() {
        RequestObjectSignUp requestObjectSignUp = new RequestObjectSignUp();
        requestObjectSignUp.setAddress(etAddress.getText().toString());
        requestObjectSignUp.setStreetLine1(etStreetLine1.getText().toString());
        requestObjectSignUp.setStreetLine2(etStreetLine2.getText().toString());
        requestObjectSignUp.setCity(etCity.getText().toString());
        requestObjectSignUp.setZipCode(etZip.getText().toString());
        requestObjectSignUp.setRegion(etRegion.getText().toString());
        requestObjectSignUp.setCountry(etCountry.getText().toString());
        requestObjectSignUp.setBirthday(etBirthday.getText().toString());
        requestObjectSignUp.setSex(spSex.getSelectedItemPosition());
        requestObjectSignUp.setReligion(etReligion.getText().toString());
        requestObjectSignUp.setMobileNumber(etContact.getText().toString());
        requestObjectSignUp.setHighSchool(etHighschool.getText().toString());
        requestObjectSignUp.setCollege(etCollege.getText().toString());
        requestObjectSignUp.setUniversity(etUniversity.getText().toString());
        requestObjectSignUp.setProfession(etProfession.getText().toString());
        //requestObjectSignUp.setBloodGroup(etStreetLine1.getText().toString());
        requestObjectSignUp.setProfileImage(BitmapHelper.getStringImage(profileImageBitmap));
        //requestObjectSignUp.setCoverImage(etStreetLine1.getText().toString());
        return requestObjectSignUp;
    }

    // populate all edittexts with data from object
    public void populateFields(RequestObjectSignUp requestObjectSignUp) {
        Log.e("Populate", "Call to populate fields");
        etAddress.setText(requestObjectSignUp.getAddress());
        etStreetLine1.setText(requestObjectSignUp.getStreetLine1());
        etStreetLine2.setText(requestObjectSignUp.getStreetLine2());
        etCity.setText(requestObjectSignUp.getCity());
        etZip.setText(requestObjectSignUp.getZipCode());
        etRegion.setText(requestObjectSignUp.getRegion());
        etCountry.setText(requestObjectSignUp.getCountry());
        // TODO: deal with birthday
        Log.e("Birthday", "Birthday received from server: " + requestObjectSignUp.getBirthday());
        spSex.setSelection(requestObjectSignUp.getSex());
        etReligion.setText(requestObjectSignUp.getReligion());
        etContact.setText(requestObjectSignUp.getMobileNumber());
        etHighschool.setText(requestObjectSignUp.getHighSchool());
        etCollege.setText(requestObjectSignUp.getCollege());
        etUniversity.setText(requestObjectSignUp.getUniversity());
        etProfession.setText(requestObjectSignUp.getProfession());
        // BloodGroup(etStreetLine1.setText(requestObjectSignUp.get());
        // TODO: set profile image from url
        profileImageView.setImageUrl(TrackURL.BASE_URL + requestObjectSignUp.getProfileImage(),
                TrackSingleton.getInstance(getActivity()).getImageLoader());
    }
}

