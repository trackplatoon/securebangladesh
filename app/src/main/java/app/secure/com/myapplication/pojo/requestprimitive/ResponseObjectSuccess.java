package app.secure.com.myapplication.pojo.requestprimitive;

import java.io.Serializable;

/**
 * Created by Saimon on 03-Jan-16.
 */
public class ResponseObjectSuccess implements Serializable{
    private boolean success;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }
}
