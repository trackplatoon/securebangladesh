package app.secure.com.myapplication.activity;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.support.v7.widget.Toolbar;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.zip.CheckedInputStream;

import app.secure.com.myapplication.R;
import app.secure.com.myapplication.dummydata.FillerTextFactory;
import app.secure.com.myapplication.pojo.requestprimitive.UserEntity;

public class TermsAndCondition extends ActionBarActivity {

    LinearLayout agreementContainer;
    Button startNow;
    Toolbar toolbar;
    CheckBox checkBox;
    TextView termsText;
    UserEntity user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms_and_condition);
        user = (UserEntity) getIntent().getSerializableExtra("FACEBOOK_USER");
        toolbar = (Toolbar) findViewById(R.id.toolbar_terms);


        if (toolbar != null) {
            toolbar.inflateMenu(R.menu.menu_terms_and_condition);
        }
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setCustomView(R.layout.custom_terms);
        termsText = (TextView) findViewById(R.id.text_terms_and_conditions);
        termsText.setText(FillerTextFactory.baseText);
        checkBox = (CheckBox) findViewById(R.id.checkbox_agreement_to_terms);
        startNow = (Button) findViewById(R.id.button_start_now);
        agreementContainer = (LinearLayout) findViewById(R.id.layout_terms_agreement);
        // if the activity is launched from navigation drawer, then hide the next button
        if(getIntent().getBooleanExtra("VIEW_ONLY", false))
            agreementContainer.setVisibility(View.GONE);
    }

    @Override
    protected void onResume(){
        super.onResume();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_terms_and_condition, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_edit_pro) {
            // TODO: // FIXME: 07-Mar-16
            startActivity(new Intent(TermsAndCondition.this, ProfilePageSetup.class));
        }

        return super.onOptionsItemSelected(item);
    }

    public void toggleAgreement(View view){
        boolean checked = ((CheckBox) view).isChecked();
        if(checked) {
            startNow.setEnabled(true);
        }
        else{
            startNow.setEnabled(false);
        }
    }

    public void startNow(View view){
        Intent intent = new Intent(TermsAndCondition.this, ProfilePageSetup.class);
        intent.putExtra("FACEBOOK_USER", user);
        startActivity(intent);
    }
}
