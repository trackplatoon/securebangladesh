package app.secure.com.myapplication.pojo.requestprimitive;

import java.io.Serializable;
import java.util.List;

import app.secure.com.myapplication.pojo.Post;

/**
 * Created by Saimon on 03-Jan-16.
 */
public class ResponseObjectFeed implements Serializable{
    private boolean success;
    private int countPost;
    private int idLoggedIn;
    private List<Post> posts;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public List<Post> getPosts() {
        return posts;
    }

    public void setPosts(List<Post> posts) {
        this.posts = posts;
    }

    public int getCountPost() {
        return countPost;
    }

    public void setCountPost(int countPost) {
        this.countPost = countPost;
    }

    public int getIdLoggedIn() {
        return idLoggedIn;
    }

    public void setIdLoggedIn(int idLoggedIn) {
        this.idLoggedIn = idLoggedIn;
    }
}
