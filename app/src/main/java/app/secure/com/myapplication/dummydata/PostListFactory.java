package app.secure.com.myapplication.dummydata;

import java.util.ArrayList;
import java.util.Random;

import app.secure.com.myapplication.pojo.Post;
import app.secure.com.myapplication.pojo.User;

/**
 * Created by Saimon on 24-Oct-15.
 */
public class PostListFactory {
    public static ArrayList<Post> getDummyPosts() {
        ArrayList<Post> postList = new ArrayList<>();
        Random rand = new Random();
        Post post;
        User user;

        // 1
        post = new Post();
        user = new User();
        user.setUsername("Emma Stone");
        post.setUser(user);
        post.setReportType(1);
        post.setPostTime("1 hour ago");
        post.setPostText(FillerTextFactory.getString());
        post.setCountSupport(rand.nextInt(300));
        post.setCountComment(rand.nextInt(300));
        post.setCountShare(rand.nextInt(300));
        post.setIsSupported(false);
        postList.add(post);


        // 2
        post = new Post();
        user = new User();
        user.setUsername("Emma Stone");
        post.setUser(user);
        post.setReportType(0);
        post.setPostTime("2 hour ago");
        post.setPostText(FillerTextFactory.getString());
        post.setCountSupport(rand.nextInt(300));
        post.setCountComment(rand.nextInt(300));
        post.setCountShare(rand.nextInt(300));
        post.setIsSupported(true);
        postList.add(post);

        // 3
        post = new Post();
        user = new User();
        user.setUsername("Emma Stone");
        post.setUser(user);
        post.setReportType(3);
        post.setPostTime("4 hour ago");
        post.setPostText(FillerTextFactory.getString());
        post.setCountSupport(rand.nextInt(300));
        post.setCountComment(rand.nextInt(300));
        post.setCountShare(rand.nextInt(300));
        post.setIsSupported(false);
        postList.add(post);

        // 4
        post = new Post();
        user = new User();
        user.setUsername("Emma Stone");
        post.setUser(user);
        post.setReportType(4);
        post.setPostTime("5 hour ago");
        post.setPostText(FillerTextFactory.getString());
        post.setCountSupport(rand.nextInt(300));
        post.setCountComment(rand.nextInt(300));
        post.setCountShare(rand.nextInt(300));
        post.setIsSupported(false);
        postList.add(post);

        // 5
        post = new Post();
        user = new User();
        user.setUsername("Emma Stone");
        post.setUser(user);
        post.setReportType(0);
        post.setPostTime("5 hour ago");
        post.setPostText(FillerTextFactory.getString());
        post.setCountSupport(rand.nextInt(300));
        post.setCountComment(rand.nextInt(300));
        post.setCountShare(rand.nextInt(300));
        post.setIsSupported(true);
        postList.add(post);

        // 6
        post = new Post();
        user = new User();
        user.setUsername("Emma Stone");
        post.setUser(user);
        post.setReportType(0);
        post.setPostTime("7 hour ago");
        post.setPostText(FillerTextFactory.getString());
        post.setCountSupport(rand.nextInt(300));
        post.setCountComment(rand.nextInt(300));
        post.setCountShare(rand.nextInt(300));
        post.setIsSupported(false);
        postList.add(post);

        return postList;
    }
}
