package app.secure.com.myapplication.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.json.JSONObject;

import app.secure.com.myapplication.R;
import app.secure.com.myapplication.fragment.EditProfileFragment;
import app.secure.com.myapplication.pojo.requestprimitive.RequestObjectSignUp;
import app.secure.com.myapplication.pojo.requestprimitive.ResponseObjectSuccess;
import app.secure.com.myapplication.network.NetworkRequestHelper;

// TODO: separate reusable UI fragment
public class EditProfile extends AppCompatActivity {

    private Toolbar toolbar;
    private Bitmap profileImageBitmap;
    private ProgressDialog progress;
    private int PICK_IMAGE_REQUEST = 1;
    private FrameLayout fragmentContainer;
    private EditProfileFragment editProfileFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            return;
        }
        setContentView(R.layout.activity_edit_profile);
        toolbar = (Toolbar) findViewById(R.id.toolbar_profile_setup);
        fragmentContainer = (FrameLayout) findViewById(R.id.fragment_container);

        if (toolbar != null) {
            toolbar.inflateMenu(R.menu.menu_profile_page_setup);
        }
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        getSupportActionBar().setCustomView(R.layout.custom_profile_setup);

        editProfileFragment = EditProfileFragment.newInstance();
        getFragmentManager().beginTransaction().add(R.id.fragment_container, editProfileFragment).commit();
        NetworkRequestHelper.startActionGetUserDetails(this, createUserDetailsGetListener(), createErrorListener());
    }

    @Override
    public void onResume() {
        super.onResume();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_profile_page_setup, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void pickProfilePhotoFromPhone(View view) {
        editProfileFragment.showFileChooser();
    }


    // collect the data from fragment and send them to server
    public void onSubmitUserDetails(View view) {
        progress = ProgressDialog.show(EditProfile.this, "Updating Your Profile",
                "please wait...", true);
        RequestObjectSignUp requestObject = editProfileFragment.collectData();
        NetworkRequestHelper.startActionUpdateUserDetails(EditProfile.this,
                requestObject,
                createUserDetailsUpdateListener(),
                createErrorListener());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent result){
        if(editProfileFragment != null)
            editProfileFragment.onActivityResult(requestCode, resultCode, result);
    }

    // TODO: Unused method? Remove.
    private void beginCrop(Uri source) {
        if(editProfileFragment != null)
            editProfileFragment.beginCrop(source);
    }

    // TODO: Unused method? Remove.
    private void handleCrop(int resultCode, Intent result){
        if(editProfileFragment != null)
            editProfileFragment.handleCrop(resultCode, result);
    }

    private Response.Listener<JSONObject> createUserDetailsGetListener() {
        return new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("RESPONSE", "response: " +response.toString());
                try {
                    ObjectMapper mapper = new ObjectMapper();
                    RequestObjectSignUp responseBody = mapper.readValue(response.toString(),
                            RequestObjectSignUp.class);
                    if (editProfileFragment != null) {
                        editProfileFragment.populateFields(responseBody);
                    }
                    else
                        Log.e("NULL", "Fragment reference is null");
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Log.e("DONE", "Outside try-catch block");
            }
        };
    }

    private Response.Listener<JSONObject> createUserDetailsUpdateListener() {
        return new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                progress.dismiss();
                try {
                    // TODO: fix the conversion from JSON to class
                    ObjectMapper mapper = new ObjectMapper();
                    ResponseObjectSuccess responseBody = mapper.readValue(response.toString(),
                            ResponseObjectSuccess.class);
                    Toast.makeText(EditProfile.this, "Saved!", Toast.LENGTH_SHORT).show();
                    finish();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
    }

    private Response.ErrorListener createErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(EditProfile.this, "Error: " + error.toString(), Toast.LENGTH_SHORT).show();
            }
        };
    }
//
//    public void onActivityResult(int requestCode, int resultCode, Intent result) {
//        if(editProfileFragment != null){
//            editProfileFragment.onActivityResult(requestCode, resultCode, result);
//        }
//    }


}
