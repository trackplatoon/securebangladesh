package app.secure.com.myapplication.pojo.requestprimitive;

/**
 * Created by Saimon on 15-Jan-16.
 */
public class RequestObjectSupportPost {
    private int postId;
    // TODO: fix boolean bug in server
    private String isSupported;

    public RequestObjectSupportPost(int postId, boolean isSuported){
        setPostId(postId);
        setIsSupported(isSuported);
    }

    public int getPostId() {
        return postId;
    }

    public void setPostId(int postId) {
        this.postId = postId;
    }

    public String getIsSupported() {
        return isSupported;
    }

    public void setIsSupported(boolean supported) {
        this.isSupported = (supported) ? "true" : "false";
    }
}
