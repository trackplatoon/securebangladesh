package app.secure.com.myapplication.pojo.requestprimitive;

/**
 * Created by Saimon on 16-Jan-16.
 */
public class RequestObjectSupportComment {
    private int commentId;
    private String isSupported;

    public RequestObjectSupportComment(int commentId, boolean isSupported){
        setCommentId(commentId);
        setIsSupported(isSupported);
    }

    public int getCommentId() {
        return commentId;
    }

    public void setCommentId(int commentId) {
        this.commentId = commentId;
    }

    public String getIsSupported() {
        return isSupported;
    }

    public void setIsSupported(boolean isSupported) {
        this.isSupported = isSupported ? "true" : "false";
    }
}
