package app.secure.com.myapplication.activity;

import android.content.Intent;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import org.w3c.dom.Text;

import app.secure.com.myapplication.R;
import app.secure.com.myapplication.dummydata.FillerTextFactory;


public class AboutActivity extends ActionBarActivity {
    private String[] menuListItem;
    private TextView aboutText;
    private Button next;
    Toolbar toolbar;
    private Boolean isViewOnly;

    Menu menu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        toolbar = (Toolbar) findViewById(R.id.toolbar_about);


        if (toolbar != null) {
            toolbar.inflateMenu(R.menu.menu_about);
        }
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setCustomView(R.layout.custom_about);
        menuListItem = getResources().getStringArray(R.array.array_incident_type);
        aboutText = (TextView) findViewById(R.id.text_about_us);
        next = (Button) findViewById(R.id.button_about_us_next);
        aboutText.setText(FillerTextFactory.baseText);
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AboutActivity.this,TermsAndCondition.class);
                startActivity(intent);
            }
        });

        // if the activity is launched from navigation drawer, then hide the next button
        isViewOnly = getIntent().getBooleanExtra("VIEW_ONLY", false);
        if(isViewOnly)
            next.setVisibility(View.GONE);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        if( !isViewOnly )
        getMenuInflater().inflate(R.menu.menu_about, menu);
//        this.menu = menu;
//        menu.getItem(0).setIcon(getResources().getDrawable(R.drawable.edit_pro_ico));
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        switch (id) {
            case R.id.action_edit_pro:
                startActivity(new Intent(AboutActivity.this, ProfilePageSetup.class));
                break;
            case android.R.id.home:
            case R.id.home:
            case R.id.homeAsUp:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }


}
