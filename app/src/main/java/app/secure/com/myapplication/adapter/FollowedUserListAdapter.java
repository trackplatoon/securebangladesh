package app.secure.com.myapplication.adapter;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import app.secure.com.myapplication.R;
import app.secure.com.myapplication.pojo.User;
import app.secure.com.myapplication.network.TrackSingleton;
import app.secure.com.myapplication.view.CircularNetworkImageView;
import app.secure.com.myapplication.network.TrackURL;

/**
 * Created by Arafat on 03/12/2015.
 */
public class FollowedUserListAdapter extends BaseAdapter {

    private ArrayList<User> followedUserList;
    private Context context;
    private LayoutInflater inflater = null;

    public FollowedUserListAdapter(Context context, ArrayList<User> friendList) {
        this.context = context;
        this.followedUserList = friendList;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return followedUserList.size();
    }

    @Override
    public Object getItem(int position) {
        return followedUserList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int pos, View view, ViewGroup viewGroup) {

        Holder holder = new Holder();
        if( view == null ) {
            view = inflater.inflate(R.layout.list_item_friend, null);
            holder.username = (TextView) view.findViewById(R.id.username);
            holder.userImage = (CircularNetworkImageView) view.findViewById(R.id.friend_avatar);
            view.setTag(holder);
        }else {
            holder = (Holder) view.getTag();
        }

        holder.username.setText(followedUserList.get(pos).getUsername());
        if( followedUserList.get(pos).getUserProfileImageUrl() != null )
            holder.userImage.setImageUrl(TrackURL.BASE_URL + followedUserList.get(pos).getUserProfileImageUrl(),
                    TrackSingleton.getInstance(context).getImageLoader());
        else
            holder.userImage.setImageBitmap(BitmapFactory.decodeResource(context.getResources(), R.drawable.avatar_small));
        return view;
    }


    public class Holder {
        TextView username;
        CircularNetworkImageView userImage;
    }
}
