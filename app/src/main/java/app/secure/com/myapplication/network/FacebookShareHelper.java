package app.secure.com.myapplication.network;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;

import org.json.JSONException;
import org.json.JSONObject;

import app.secure.com.myapplication.pojo.Post;
import app.secure.com.myapplication.network.NetworkRequestHelper;
import app.secure.com.myapplication.pojo.requestprimitive.UserEntity;

/**
 * Created by Saimon on 30-Jan-16.
 */
public class FacebookShareHelper {
    private static CallbackManager fbManager;

    public static CallbackManager init(Activity c) {
        if (!FacebookSdk.isInitialized()) {
            FacebookSdk.sdkInitialize(c);
        }
        return fbManager = CallbackManager.Factory.create();
    }


    public static void share(final Activity activity, final Post post, final String link) {
        ShareDialog shareDialog = new ShareDialog(activity);
        shareDialog.registerCallback(fbManager, new FacebookCallback<Sharer.Result>() {
            @Override
            public void onSuccess(Sharer.Result result) {
                Toast.makeText(activity, "You shared this post", Toast.LENGTH_SHORT).show();
                NetworkRequestHelper.startActionSharePost(activity, post.getPostId());
            }

            @Override
            public void onCancel() {
            }

            @Override
            public void onError(FacebookException e) {
                e.printStackTrace();
            }
        });
        ShareLinkContent content = new ShareLinkContent.Builder()
                .setContentTitle("Track Platoon")
                .setContentDescription(post.getPostText())
                .setContentUrl(Uri.parse(link))
                .build();
        shareDialog.show(content);
    }
}
