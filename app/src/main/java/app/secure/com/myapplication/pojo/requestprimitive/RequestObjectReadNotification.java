package app.secure.com.myapplication.pojo.requestprimitive;

/**
 * Created by Saimon on 31-Jan-16.
 */
public class RequestObjectReadNotification {
    private int notificationId;

    public  RequestObjectReadNotification(int notificationId){
        setNotificationId(notificationId);
    }

    public int getNotificationId() {
        return notificationId;
    }

    public void setNotificationId(int notificationId) {
        this.notificationId = notificationId;
    }
}
