package app.secure.com.myapplication.fragment;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.facebook.FacebookSdk;
import com.facebook.share.widget.ShareDialog;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import app.secure.com.myapplication.R;
import app.secure.com.myapplication.activity.ViewPostActivity;
import app.secure.com.myapplication.adapter.CustomSpinnerAdapter;
import app.secure.com.myapplication.adapter.PostAdapter;
import app.secure.com.myapplication.global.TrackPlatoon;
import app.secure.com.myapplication.listener.EndlessScrollListener;
import app.secure.com.myapplication.pojo.Post;
import app.secure.com.myapplication.pojo.requestprimitive.ResponseObjectFeed;
import app.secure.com.myapplication.network.NetworkRequestHelper;
import app.secure.com.myapplication.network.FacebookShareHelper;

/**
 * Created by Saimon on 16-Oct-15.
 */
public class NewsfeedFragment extends Fragment{
    private ListView postListView;
    private PostAdapter mAdapter;
    private Spinner spinner;
    private ArrayList<Post> postList;
    private int sharedPostId = 0;
    ShareDialog shareDialog;

    // Resources should be accessed from values, but context issues must be resolved in order to do that.
    //private String[] objects = getContext().getResources().getStringArray(R.array.newsfeed_filter_options);
    private String[] objects = {"All", "Report", "Help request"};

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        FacebookSdk.sdkInitialize(getActivity());
        View rootView = inflater.inflate(R.layout.fragment_newsfeed, container, false);
        postListView = (ListView) rootView.findViewById(R.id.listview_newsfeed);
        spinner = (Spinner) rootView.findViewById(R.id.spinner_newsfeed);
        spinner.setAdapter(new CustomSpinnerAdapter(getActivity(), R.id.text_spinner_selected, objects));
        postListView.setOnScrollListener(new EndlessScrollListener() {
            @Override
            public boolean onLoadMore(int page, int totalItemsCount) {
                // Triggered only when new data needs to be appended to the list
                // Add whatever code is needed to append new items to your AdapterView
                customLoadMoreDataFromApi(page);
                // or customLoadMoreDataFromApi(totalItemsCount);
                return true; // ONLY if more data is actually being loaded; false otherwise.
            }
        });
        postList = new ArrayList<>();
        mAdapter = new PostAdapter(getActivity(), postList);
        postListView.setAdapter(mAdapter);
        try {
            NetworkRequestHelper.startActionGetFeed(getContext(),1, createFeedReqListener(), createReqErrorListener());
        }catch (NullPointerException e){e.printStackTrace();}
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    // creates and returns a listener that handles the response to notification request
    private Response.Listener<JSONObject> createFeedReqListener() {
        return new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    // TODO: fix the conversion from JSON to class
                    ObjectMapper mapper = new ObjectMapper();
                    ResponseObjectFeed responseBody = mapper.readValue(response.toString(),
                            ResponseObjectFeed.class);
                    List<Post> newPosts = (ArrayList) responseBody.getPosts();
                    postList.addAll(newPosts);
                    if (getActivity() == null)
                        Log.e("NewsfeedFragment", "After receiving response Hosting activity is null");
                    if (mAdapter == null) {
                        mAdapter = new PostAdapter(getActivity(), postList);
                        postListView.setAdapter(mAdapter);
                    }
                    mAdapter.notifyDataSetChanged();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
    }

    private Response.ErrorListener createReqErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getActivity(), "Server Error: " + error.getMessage(), Toast.LENGTH_SHORT);
            }
        };
    }

    public void handleSupportPostClick(View v) {
        int position = (int) v.getTag();
        try {
            postList.get(position).toggleSupport();
            mAdapter.notifyDataSetChanged();
            NetworkRequestHelper.startActionSupportPost(getActivity(),
                    postList.get(position).getPostId(),
                    postList.get(position).isSupported());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void handleCommentClick(View v) {
        int position = (int) v.getTag();
        Intent intent = new Intent(this.getActivity(), ViewPostActivity.class);
        Log.e("commentClick", "3: initiated intent");
        intent.putExtra("POST_ID", postList.get(position).getPostId());
        intent.putExtra("POST", postList.get(position));
        startActivity(intent);
    }

    private void customLoadMoreDataFromApi(int page){
        try {
            NetworkRequestHelper.startActionGetFeed(getContext(), page ,createFeedReqListener(), createReqErrorListener());
        }catch (NullPointerException e){e.printStackTrace();}
    };

    public void sharePostClick(View v) {
        Log.e("sharePostClick", "1. Method entry point");
        Post post = postList.get((int) v.getTag());
        FacebookShareHelper.share(getActivity(),
                post,
                "http://www.trackplatoon.com");
    }

}
