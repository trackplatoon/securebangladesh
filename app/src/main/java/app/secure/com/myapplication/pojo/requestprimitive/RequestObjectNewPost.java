package app.secure.com.myapplication.pojo.requestprimitive;

import app.secure.com.myapplication.pojo.Post;

/**
 * Created by Saimon on 03-Jan-16.
 */
public class RequestObjectNewPost {
    private int reportType;
    private String postText;
    private String postImagePrimary;
    private String isAnonymous;

    public RequestObjectNewPost(Post post){
        setPostText(post.getPostText());
        setReportType(post.getReportType());
        setIsAnonymous(post.getIsAnonymous());
        setPostImagePrimary(post.getPostImagePrimary());
    }

    public int getReportType() {
        return reportType;
    }

    public void setReportType(int reportType) {
        this.reportType = reportType;
    }

    public String getPostText() {
        return postText;
    }

    public void setPostText(String postText) {
        this.postText = postText;
    }

    public String getPostImagePrimary() {
        return postImagePrimary;
    }

    public void setPostImagePrimary(String postImagePrimary) {
        this.postImagePrimary = postImagePrimary;
    }

    public String getIsAnonymous() {
        return isAnonymous;
    }

    public void setIsAnonymous(boolean isAnonymous) {
        this.isAnonymous = isAnonymous ? "true" : "false" ;
    }
}
