package app.secure.com.myapplication.pojo.requestprimitive;

import java.io.Serializable;
import java.util.ArrayList;

import app.secure.com.myapplication.pojo.Comment;
import app.secure.com.myapplication.pojo.Post;

/**
 * Created by Saimon on 03-Jan-16.
 */
public class ResponseObjectViewPost implements Serializable{
    private boolean success;
    private Post post;
    private ArrayList<Comment> comments;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public Post getData() {
        return post;
    }

    public void setData(Post post) {
        this.post = post;
    }

    public ArrayList<Comment> getComments() {
        return comments;
    }

    public void setComments(ArrayList<Comment> comments) {
        this.comments = comments;
    }
}
