package app.secure.com.myapplication.network;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import app.secure.com.myapplication.global.TrackPlatoon;
import app.secure.com.myapplication.pojo.Comment;
import app.secure.com.myapplication.pojo.Post;
import app.secure.com.myapplication.pojo.requestprimitive.RequestObjectFollowUser;
import app.secure.com.myapplication.pojo.requestprimitive.RequestObjectNewComment;
import app.secure.com.myapplication.pojo.requestprimitive.RequestObjectNewPost;
import app.secure.com.myapplication.pojo.requestprimitive.RequestObjectReadNotification;
import app.secure.com.myapplication.pojo.requestprimitive.RequestObjectSharePost;
import app.secure.com.myapplication.pojo.requestprimitive.RequestObjectSignUp;
import app.secure.com.myapplication.pojo.requestprimitive.RequestObjectSupportComment;
import app.secure.com.myapplication.pojo.requestprimitive.RequestObjectSupportPost;
import app.secure.com.myapplication.pojo.requestprimitive.ResponseObjectSuccess;
import app.secure.com.myapplication.pojo.requestprimitive.UserEntity;

// takes parameters from calling activity, packs them into request objects and adds them to Singleton requestQueue
public class NetworkRequestHelper {

    public static void startActionGetFeed(Context context,
                                          int page,
                                          Response.Listener<JSONObject> feedReqListener,
                                          Response.ErrorListener errorListener) {
        String accessToken = (((TrackPlatoon) context.getApplicationContext()).getCredentials()).getToken();
        String url = TrackURL.URL_FEED + "?access%20token=" + accessToken + "&page=" + page;
        Log.e("Volley URL", url);
        JsonObjectRequest myReq = new JsonObjectRequest(Request.Method.GET,
                url,
                null,
                feedReqListener,
                errorListener);
        TrackSingleton.getInstance(context).getRequestQueue().add(myReq);
    }

    public static void startActionDiscoverPeople(Context context,
                                          int page,
                                          Response.Listener<JSONObject> discoverPeopleListener,
                                          Response.ErrorListener errorListener) {
        String accessToken = (((TrackPlatoon) context.getApplicationContext()).getCredentials()).getToken();
        String url = TrackURL.URL_DISCOVER_PEOPLE + "?access%20token=" + accessToken + "&page=" + page;
        Log.e("Volley URL", url);
        JsonObjectRequest myReq = new JsonObjectRequest(Request.Method.GET,
                url,
                null,
                discoverPeopleListener,
                errorListener);
        TrackSingleton.getInstance(context).getRequestQueue().add(myReq);
    }

    public static void startActionGetDashboard(Context context,
                                          Response.Listener<JSONObject> dashboardReqListener,
                                          Response.ErrorListener errorListener) {
        String accessToken = (((TrackPlatoon) context.getApplicationContext()).getCredentials()).getToken();
        String url = TrackURL.URL_DASHBOARD + "?access%20token=" + accessToken;
        Log.e("Volley URL", url);
        JsonObjectRequest myReq = new JsonObjectRequest(Request.Method.GET,
                url,
                null,
                dashboardReqListener,
                errorListener);
        TrackSingleton.getInstance(context).getRequestQueue().add(myReq);
    }

    public static void startActionGetNotifications(Context context,
                                                   Response.Listener<JSONObject> notificationReqListener,
                                                   Response.ErrorListener errorListener) {
        String accessToken = (((TrackPlatoon) context.getApplicationContext()).getCredentials()).getToken();
        String url = TrackURL.URL_NOTIFICATION + "?access%20token=" + accessToken;
        Log.e("Volley URL", url);
        JsonObjectRequest myReq = new JsonObjectRequest(Request.Method.GET,
                url,
                null,
                notificationReqListener,
                errorListener);
        TrackSingleton.getInstance(context).getRequestQueue().add(myReq);
    }

    public static void startActionGetFollowList(Context context,
                                                 Response.Listener<JSONObject> followListReqListener,
                                                 Response.ErrorListener errorListener) {
        String accessToken = (((TrackPlatoon) context.getApplicationContext()).getCredentials()).getToken();
        String url = TrackURL.URL_VIEWFOLLOWING + "?access%20token=" + accessToken;
        Log.e("Volley URL", url);
        JsonObjectRequest myReq = new JsonObjectRequest(Request.Method.GET,
                url,
                null,
                followListReqListener,
                errorListener);
        TrackSingleton.getInstance(context).getRequestQueue().add(myReq);
    }

    public static void startActionGetUserDetails(Context context,
                                                Response.Listener<JSONObject> userDetailsGetReqListener,
                                                Response.ErrorListener errorListener) {
        String accessToken = (((TrackPlatoon) context.getApplicationContext()).getCredentials()).getToken();
        String url = TrackURL.URL_GETUSERDETAILS + "?access%20token=" + accessToken;
        Log.e("Volley URL", url);
        JsonObjectRequest myReq = new JsonObjectRequest(Request.Method.GET,
                url,
                null,
                userDetailsGetReqListener,
                errorListener);
        TrackSingleton.getInstance(context).getRequestQueue().add(myReq);
    }

    public static void startActionUpdateUserDetails(Context context,
                                                    RequestObjectSignUp requestObject,
                                                 Response.Listener<JSONObject> userDetailsUpdateReqListener,
                                                 Response.ErrorListener errorListener) {
        String accessToken = (((TrackPlatoon) context.getApplicationContext()).getCredentials()).getToken();
        String url = TrackURL.URL_UPDATEUSERDETAILS + "?access%20token=" + accessToken;
        String requestString;
        JSONObject jsonObject = null;
        try {
            requestString = getMapper().writeValueAsString(requestObject);
            jsonObject = new JSONObject(requestString);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.e("Volley Req", jsonObject.toString());
        JsonObjectRequest myReq = new JsonObjectRequest(Request.Method.POST,
                url,
                jsonObject,
                userDetailsUpdateReqListener,
                errorListener);
        TrackSingleton.getInstance(context).getRequestQueue().add(myReq);
    }

    public static void startActionNewPost(Context context,
                                          Post post,
                                          Response.Listener<JSONObject> newPostReqListener,
                                          Response.ErrorListener errorListener) {
        String accessToken = (((TrackPlatoon) context.getApplicationContext()).getCredentials()).getToken();
        String url = TrackURL.URL_NEWPOST + "?access%20token=" + accessToken;
        String requestString = null;
        JSONObject jsonObject = null;
        // TODO: param userId not needed?
        Log.e("DEBUG", "Am I anonymous? " + post.getIsAnonymous());
        Log.e("DEBUG", "Image: " + post.getPostImagePrimary());
        RequestObjectNewPost requestBody = new RequestObjectNewPost(post);
        try {
            requestString = getMapper().writeValueAsString(requestBody);
            jsonObject = new JSONObject(requestString);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.e("Volley URL", url);
        Log.e("Volley Req", jsonObject.toString());
        JsonObjectRequest myReq = new JsonObjectRequest(Request.Method.POST,
                url,
                jsonObject,
                newPostReqListener,
                errorListener);
        TrackSingleton.getInstance(context).getRequestQueue().add(myReq);
    }

    public static void startActionViewPost(Context context,
                                           int postId,
                                           Response.Listener<JSONObject> viewPostReqListener,
                                           Response.ErrorListener errorListener) {
        JSONObject jsonObject = null;
        String accessToken = (((TrackPlatoon) context.getApplicationContext()).getCredentials()).getToken();
        String url = TrackURL.URL_VIEWPOST +postId + "?access%20token=" + accessToken;
        Log.e("Volley URL", url);
        JsonObjectRequest myReq = new JsonObjectRequest(Request.Method.GET,
                url,
                jsonObject,
                viewPostReqListener,
                errorListener);
        TrackSingleton.getInstance(context).getRequestQueue().add(myReq);
    }

    public static void startActionViewUser(Context context,
                                           int userId,
                                           Response.Listener<JSONObject> viewUserReqListener,
                                           Response.ErrorListener errorListener) {
        JSONObject jsonObject = null;
        String accessToken = (((TrackPlatoon) context.getApplicationContext()).getCredentials()).getToken();
        String url = TrackURL.URL_VIEWUSER + userId + "?access%20token=" + accessToken;
        Log.e("Volley URL", url);
        JsonObjectRequest myReq = new JsonObjectRequest(Request.Method.GET,
                url,
                null,
                viewUserReqListener,
                errorListener);
        TrackSingleton.getInstance(context).getRequestQueue().add(myReq);
    }

    public static void startActionViewPostByUser(Context context,
                                                 int userId,
                                                 int page,
                                                 Response.Listener<JSONObject> userPostReqListener,
                                                 Response.ErrorListener errorListener) {
        JSONObject jsonObject = null;
        String accessToken = (((TrackPlatoon) context.getApplicationContext()).getCredentials()).getToken();
        String url = TrackURL.URL_VIEWUSERPOSTS + userId + "/posts" + "?access%20token=" + accessToken + "&page=" + page;
        Log.e("Volley URL", url);
        JsonObjectRequest myReq = new JsonObjectRequest(Request.Method.GET,
                url,
                null,
                userPostReqListener,
                errorListener);
        TrackSingleton.getInstance(context).getRequestQueue().add(myReq);
    }

    public static void startActionNewComment(Context context,
                                             int postId,
                                             Comment comment,
                                             Response.Listener<JSONObject> newCommentReqListener,
                                             Response.ErrorListener errorListener) {
        String accessToken = (((TrackPlatoon) context.getApplicationContext()).getCredentials()).getToken();
        String url = TrackURL.URL_NEWCOMMENT+ "?access%20token=" + accessToken;
        String requestString = null;
        JSONObject jsonObject = null;
        // TODO: param userId not needed?
        RequestObjectNewComment requestBody = new RequestObjectNewComment(postId, comment.getCommentText());
        try {
            requestString = getMapper().writeValueAsString(requestBody);
            jsonObject = new JSONObject(requestString);

        Log.e("Volley URL", url);
        Log.e("Volley Req", jsonObject.toString());
        JsonObjectRequest myReq = new JsonObjectRequest(Request.Method.POST,
                url,
                jsonObject,
                newCommentReqListener,
                errorListener);
        TrackSingleton.getInstance(context).getRequestQueue().add(myReq);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void startActionSupportPost(Context context, int postId, boolean isSupported) {
        String accessToken = (((TrackPlatoon) context.getApplicationContext()).getCredentials()).getToken();
        String url = TrackURL.URL_SUPPORTPOST + "?access%20token=" + accessToken;
        String requestString = null;
        JSONObject jsonObject = null;
        RequestObjectSupportPost requestBody = new RequestObjectSupportPost(postId, isSupported);
        try {
            requestString = getMapper().writeValueAsString(requestBody);
            jsonObject = new JSONObject(requestString);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.e("Volley URL", url);
        Log.e("Volley Req", jsonObject.toString());
        JsonObjectRequest myReq = new JsonObjectRequest(Request.Method.POST,
                url,
                jsonObject,
                getBlankResponseListener(),
                getBlankErrorListener());
        TrackSingleton.getInstance(context).getRequestQueue().add(myReq);
    }

    public static void startActionSupportComment(Context context, int commentId, boolean isSupported) {
        String accessToken = (((TrackPlatoon) context.getApplicationContext()).getCredentials()).getToken();
        String url = TrackURL.URL_SUPPORTCOMMENT + "?access%20token=" + accessToken;
        String requestString = null;
        JSONObject jsonObject = null;
        RequestObjectSupportComment requestBody = new RequestObjectSupportComment(commentId, isSupported);
        try {
            requestString = getMapper().writeValueAsString(requestBody);
            jsonObject = new JSONObject(requestString);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.e("Volley URL", url);
        Log.e("Volley Request", jsonObject.toString());
        JsonObjectRequest myReq = new JsonObjectRequest(Request.Method.POST,
                url,
                jsonObject,
                getBlankResponseListener(),
                getBlankErrorListener());
        TrackSingleton.getInstance(context).getRequestQueue().add(myReq);
    }

    public static void startActionSharePost(Context context, int postId) {
        String accessToken = (((TrackPlatoon) context.getApplicationContext()).getCredentials()).getToken();
        String url = TrackURL.URL_SHAREPOST + postId + "?access%20token=" + accessToken;
        String requestString = null;
        JSONObject jsonObject = null;
        RequestObjectSharePost requestBody = new RequestObjectSharePost(postId);
        try {
            requestString = getMapper().writeValueAsString(requestBody);
            jsonObject = new JSONObject(requestString);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.e("Volley URL", url);
        Log.e("Volley Req", jsonObject.toString());
        JsonObjectRequest myReq = new JsonObjectRequest(Request.Method.POST,
                url,
                null,
                getBlankResponseListener(),
                getBlankErrorListener());
        TrackSingleton.getInstance(context).getRequestQueue().add(myReq);
    }

    public static void startActionReadNotification(Context context, int notificationId) {
        String accessToken = (((TrackPlatoon) context.getApplicationContext()).getCredentials()).getToken();
        String url = TrackURL.URL_READNOTIFICATION + "?access%20token=" + accessToken;
        Log.e("Volley URL", url);
        String requestString = null;
        JSONObject jsonObject = null;
        RequestObjectReadNotification requestBody = new RequestObjectReadNotification(notificationId);
        try {
            requestString = getMapper().writeValueAsString(requestBody);
            jsonObject = new JSONObject(requestString);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.e("Volley URL", url);
        Log.e("Volley Req", jsonObject.toString());
        JsonObjectRequest myReq = new JsonObjectRequest(Request.Method.POST,
                url,
                jsonObject,
                getBlankResponseListener(),
                getBlankErrorListener());
        TrackSingleton.getInstance(context).getRequestQueue().add(myReq);
    }

    public static void startActionFollowUser(Context context, int userId, boolean isFollowed) {
        String accessToken = (((TrackPlatoon) context.getApplicationContext()).getCredentials()).getToken();
        String url = TrackURL.URL_FOLLOWUSER + "?access%20token=" + accessToken;
        String requestString = null;
        JSONObject jsonObject = null;
        RequestObjectFollowUser requestBody = new RequestObjectFollowUser(userId, isFollowed);
        try {
            requestString = getMapper().writeValueAsString(requestBody);
            jsonObject = new JSONObject(requestString);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.e("Volley URL", url);
        Log.e("Volley Req", jsonObject.toString());
        JsonObjectRequest myReq = new JsonObjectRequest(Request.Method.POST,
                url,
                jsonObject,
                getBlankResponseListener(),
                getBlankErrorListener());
        TrackSingleton.getInstance(context).getRequestQueue().add(myReq);
    }

    public static void startActionLogin(Context context,
                                        final UserEntity userEntity,
                                        Response.Listener<String> loginReqListener,
                                        Response.ErrorListener errorListener) {
        Log.e("AUTH", "Call to startActionLogin()");
        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                TrackURL.URL_LOGIN,
                loginReqListener,
                errorListener) {

            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=UTF-8";
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("grant_type", "password");
                params.put("client_id", TrackURL.CLIENT_ID);
                params.put("client_secret", TrackURL.CLIENT_SECRET);
                params.put("username", userEntity.getEmail());
                params.put("password", userEntity.getPassword());
                return params;
            }

        };
        TrackSingleton.getInstance(context).getRequestQueue().add(stringRequest);
    }


    public static void startActionSignUp(final Context context,
                                         final RequestObjectSignUp requestObject,
                                         final Response.Listener<String> signUpReqListener,
                                         final Response.ErrorListener errorListener) {
        Log.e("AUTH", "Call to startActionSignUp()");
        String requestString = null;
        JSONObject jsonObject = null;
        Log.e("DEBUG", "2. Username in helper class: " + requestObject.getUsername());
        try {
            requestString = getMapper().writeValueAsString(requestObject);
            jsonObject = new JSONObject(requestString);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.e("AUTH", "Sign up sent data: " + requestString);
        Log.e("Volley URL", TrackURL.URL_SIGNUP);
        JsonObjectRequest myReq = new JsonObjectRequest(Request.Method.POST,
                TrackURL.URL_SIGNUP,
                jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        ResponseObjectSuccess responseBody;
                        Log.e("AUTH", "Sign up response from server: " +response.toString());
                        try {
                            responseBody = getMapper().readValue(response.toString(), ResponseObjectSuccess.class);
                        }catch (IOException e){
                            e.printStackTrace();
                            responseBody = new ResponseObjectSuccess();
                            responseBody.setSuccess(false);
                        }
                        if(responseBody.isSuccess()){
                            Log.e("AUTH", "StartActionSignUp: signed up successfully");
                            UserEntity user = new UserEntity(requestObject.getUsername(),
                                    requestObject.getEmail(), requestObject.getPassword());
                            startActionLogin(context, user, signUpReqListener, errorListener);
                        }else{
                            Log.e("AUTH", "Sign up failed");
                            Toast.makeText(context, "Error: sign up failed, please try again later", Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                errorListener);
        TrackSingleton.getInstance(context).getRequestQueue().add(myReq);
    }

    private static ObjectMapper getMapper(){
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.INDENT_OUTPUT, true);
        mapper.configure(SerializationFeature.WRITE_NULL_MAP_VALUES, true);
        mapper.configure(SerializationFeature.ORDER_MAP_ENTRIES_BY_KEYS, true);
        return mapper;
    }

    private static Response.Listener<JSONObject> getBlankResponseListener(){
        return new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                // do nothing
                Log.e("Volley Response", response.toString());
            }
        };
    }

    private static Response.ErrorListener getBlankErrorListener(){
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // do nothing
                Log.e("Volley Response", error.toString());
            }
        };
    }
}