package app.secure.com.myapplication.pojo;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Created by Saimon on 25-Dec-15.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TrackRequestBody {
    private String userId;
    private String token;
    private String postId;
    private String targetUserId;
    private String commentText;

    private Post post;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getPostId() {
        return postId;
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }

    public String getTargetUserId() {
        return targetUserId;
    }

    public void setTargetUserId(String targetUserId) {
        this.targetUserId = targetUserId;
    }

    public String getCommentText() {
        return commentText;
    }

    public void setCommentText(String commentText) {
        this.commentText = commentText;
    }

    public Post getPost() {
        return post;
    }

    public void setPost(Post post) {
        this.post = post;
    }
}
