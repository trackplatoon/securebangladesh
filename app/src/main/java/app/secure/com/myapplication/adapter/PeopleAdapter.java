package app.secure.com.myapplication.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import com.android.volley.toolbox.ImageLoader;

import java.util.ArrayList;

import app.secure.com.myapplication.R;
import app.secure.com.myapplication.network.TrackSingleton;
import app.secure.com.myapplication.network.TrackURL;
import app.secure.com.myapplication.pojo.User;
import app.secure.com.myapplication.view.CircularNetworkImageView;

/**
 * Created by Saimon on 03-Dec-15.
 */
public class PeopleAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<User> users;
    private ImageLoader imageLoader;

    public PeopleAdapter(Context context, ArrayList<User> users) {
        this.context = context;
        this.users = users;
        imageLoader = TrackSingleton.getInstance(context).getImageLoader();
    }

    public int getCount() {
        return 18;
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    // create a new ImageView for each item referenced by the Adapter
    public View getView(int position, View convertView, ViewGroup parent) {
        CircularNetworkImageView imageView;
        if (convertView == null) {
            // if it's not recycled, initialize some attributes
            imageView = new CircularNetworkImageView(context);
            imageView.setLayoutParams(new GridView.LayoutParams(96, 96));
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);

        } else {
            imageView = (CircularNetworkImageView) convertView;
        }
        User user = users.get(position);
        if(user.getUserProfileImageUrl()!= null && user.getUserProfileImageUrl() != ""){
            imageView.setImageUrl(TrackURL.BASE_URL + user.getUserProfileImageUrl(), imageLoader);
            imageView.setTag(user.getUserId());
        } else
            imageView.setBackground(context.getResources().getDrawable(R.color.tabmenu_bg));
        return imageView;
    }
}
