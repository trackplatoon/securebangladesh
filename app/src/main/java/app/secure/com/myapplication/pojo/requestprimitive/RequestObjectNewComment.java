package app.secure.com.myapplication.pojo.requestprimitive;

/**
 * Created by Saimon on 05-Jan-16.
 */
public class RequestObjectNewComment {
    private int postId;
    private String commentText;

    public RequestObjectNewComment(int postId, String commentText){
        setPostId(postId);
        setCommentText(commentText);
    }

    public int getPostId() {
        return postId;
    }

    public void setPostId(int postId) {
        this.postId = postId;
    }

    public String getCommentText() {
        return commentText;
    }

    public void setCommentText(String commentText) {
        this.commentText = commentText;
    }
}
