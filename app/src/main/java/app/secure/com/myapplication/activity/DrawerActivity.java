package app.secure.com.myapplication.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import app.secure.com.myapplication.R;
import app.secure.com.myapplication.adapter.MainDrawerAdapter;
import app.secure.com.myapplication.customtoolbar.EnhancedMenuInflater;
import app.secure.com.myapplication.customtoolbar.SplitToolbar;
import app.secure.com.myapplication.global.TrackPlatoon;
import app.secure.com.myapplication.network.NetworkRequestHelper;
import app.secure.com.myapplication.pojo.DrawerItem;
import app.secure.com.myapplication.pojo.requestprimitive.ResponseObjectDashboard;

public class DrawerActivity extends AppCompatActivity {
    private ActionBarDrawerToggle mDrawerToggle;
    private MainDrawerAdapter drawerAdapter;
    private List<DrawerItem> drawerItemList;
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        NetworkRequestHelper.startActionGetDashboard(this, createDashboardReqListener(), createReqErrorListener());

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (toolbar == null || true) {
            EnhancedMenuInflater.inflate(getMenuInflater(), menu, true);
        }
        return super.onCreateOptionsMenu(menu);
    }

    //Trying to hide the toolbar when drawer is open
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if (mDrawerLayout.isDrawerOpen(mDrawerList)) {
            menu.clear();
        }
        return true;
    }

    @Override
    protected void onResume(){
        super.onResume();
        mDrawerLayout.closeDrawer(Gravity.LEFT);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_drawer) {
            // open/close drawer
            invalidateOptionsMenu();
            if (mDrawerLayout.isDrawerOpen(mDrawerList))
                mDrawerLayout.closeDrawer(mDrawerList);
            else {
                mDrawerLayout.openDrawer(mDrawerList);
            }
        } else if (id == R.id.action_home) {
            // goto home page
            Intent intent = new Intent(DrawerActivity.this, HomeActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
            return true;
        } else if (id == R.id.action_post) {
            // new post
            Intent intent = new Intent(DrawerActivity.this, NewPostActivity.class);
            startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    protected void init(){
        toolbar = (SplitToolbar) findViewById(R.id.toolbar);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.drawer);
        inflateToolbar();
        initDrawer();
    }

    protected void initDrawer(){
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setHomeButtonEnabled(false);

        // add items to the navigation drawer menu list
        drawerItemList = new ArrayList<DrawerItem>();
        drawerItemList.add(new DrawerItem(null));
        drawerItemList.add(new DrawerItem("Notification", R.drawable.notification_ico, 0));
        drawerItemList.add(new DrawerItem("Settings", R.drawable.setting_ico));
        drawerItemList.add(new DrawerItem("Log out", R.drawable.log_out_ico));

        // add onItemClickListener to drawer list
        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            Intent intent;
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                        // Own profile
                        Log.e("View User", "Started ProfileActivity from drawer icon");
                        intent = new Intent(DrawerActivity.this, ProfileActivity.class);
                        startActivity(intent);
                        break;
                    case 1:
                        // Notifications
                        intent = new Intent(DrawerActivity.this, NotificationActivity.class);
                        startActivity(intent);
                        break;
                    case 2:
                        // Edit profile information
                        intent = new Intent(DrawerActivity.this, EditProfile.class);
                        startActivity(intent);
                        break;
                    case 3:
                        // Log out
                        ((TrackPlatoon)getApplication()).setCredentials(0, null); //Invalidate saved credentials
                        intent = new Intent(DrawerActivity.this, SignInActivityNew.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish();
                        break;
                }
            }
        });

        // initialize navigation drawer adapter
        drawerAdapter = new MainDrawerAdapter(this, R.layout.list_item_drawer, drawerItemList);
        mDrawerList.setAdapter(drawerAdapter);
        mDrawerToggle = new ActionBarDrawerToggle(
                this,                 // host Activity
                mDrawerLayout,        // DrawerLayout object
                toolbar,              // Toolbar
                R.string.drawer_open,               // "open drawer" description
                R.string.drawer_close       // "close drawer" description
        ) {
            // Called when a drawer has settled in a completely closed state.
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                syncState();

            }

            // Called when a drawer has settled in a completely open state.
            public void onDrawerOpened(View view) {
                super.onDrawerOpened(view);
                // bring the drawer to front so it receives click events
                mDrawerList.bringToFront();
                mDrawerLayout.requestLayout();
                syncState();
            }
        };

        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerToggle.setDrawerIndicatorEnabled(false);
        mDrawerToggle.syncState();
    }

    protected void inflateToolbar(){
        //Toolbar initialization
        if (toolbar != null) {
            EnhancedMenuInflater.inflate(getMenuInflater(), toolbar.getMenu(), true);
            toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {
                    return onOptionsItemSelected(item);
                }
            });
        }
    }

    // creates and returns a listener that handles the response to new post request
    private Response.Listener<JSONObject> createDashboardReqListener() {
        return new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    Log.e("DEBUG", "Handling response");
                    // TODO: fix the conversion from JSON to class
                    ObjectMapper mapper = new ObjectMapper();
                    ResponseObjectDashboard responseBody = mapper.readValue(response.toString(),
                            ResponseObjectDashboard.class);
                    if(responseBody != null )
                        setDashboard(responseBody);
                    else
                        Toast.makeText(DrawerActivity.this, "Error: could not connect to server", Toast.LENGTH_SHORT).show();
                        //finish();
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(DrawerActivity.this, "Error: could not connect to server", Toast.LENGTH_SHORT).show();
                }
            }
        };
    }

    private Response.ErrorListener createReqErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(DrawerActivity.this, "Error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        };
    }

    private void setDashboard(ResponseObjectDashboard response){
        ((TrackPlatoon) getApplication()).setCredentials(response.getUserId(),
                (((TrackPlatoon) getApplication()).getCredentials()).getToken());
        drawerItemList.get(0).setUserProfileImageUrl(response.getUserProfileImageUrl());
        drawerItemList.get(1).setNotificationCount(response.getUnreadNotifications());
        drawerAdapter.notifyDataSetChanged();
    }
}
