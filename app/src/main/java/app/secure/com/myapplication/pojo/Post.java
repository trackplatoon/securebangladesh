package app.secure.com.myapplication.pojo;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;

/**
 * Created by Saimon on 24-Oct-15.
 */
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class Post implements Serializable {
    private int postId;
    private User user;
    private int reportType;
    private String postTime;
    private String postText;
    private Boolean isAnonymous;
    private int countSupport;
    private int countComment;
    private int countShare;
    private int supportGoal;
    private boolean isSupported;
    private String postImagePrimary;

    private int userId;
    private String userProfileImageUrl;
    private String username;

    public int getReportType() {
        return reportType;
    }

    public void setReportType(int reportType) {
        this.reportType = reportType;
    }

    public String getPostTime() {
        return postTime;
    }

    public void setPostTime(String postTime) {
        this.postTime = postTime;
    }

    public String getPostText() {
        return postText;
    }

    public void setPostText(String postText) {
        this.postText = postText;
    }

    public int getCountSupport() {
        return countSupport;
    }

    public void setCountSupport(int countSupport) {
        this.countSupport = countSupport;
    }

    public int getCountComment() {
        return countComment;
    }

    public void setCountComment(int countComment) {
        this.countComment = countComment;
    }

    public int getCountShare() {
        return countShare;
    }

    public void setCountShare(int countShare) {
        this.countShare = countShare;
    }

    public boolean isSupported() {
        return isSupported;
    }

    public void setIsSupported(boolean isSupported) {
        this.isSupported = isSupported;
    }

    public int getPostId() {
        return postId;
    }

    public void setPostId(int postId) {
        this.postId = postId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUserProfileImageUrl() {
        return userProfileImageUrl;
    }

    public void setUserProfileImageUrl(String userProfileImageUrl) {
        this.userProfileImageUrl = userProfileImageUrl;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPostTypeAsString() {
        switch (this.reportType) {
            case 1:
                return "Asked for help";
            case 2:
                return "Created a topic";
            default:
                return "Reported against";
        }
    }

    public String getReportTypeAsString() {
        if (this.reportType > 50)
            switch (this.reportType) {
                case 51:
                    return "Petty Corruption";
                case 52:
                    return "Grand Corruption";
                case 53:
                    return "Systemic Corruption";
                case 54:
                    return "Political Corruption";
                case 55:
                    return "Police Corruption";
                case 56:
                    return "Judicial Corruption";
                case 57:
                    return "Other Corruption";
                case 58:
                    return "Murder";
                case 59:
                    return "Robbery";
                case 60:
                    return "Thiefting";
                case 61:
                    return "Sexual Harassment";
                case 62:
                    return "Drug Trade";
                case 63:
                    return "Illegal Weapon";
                case 64:
                    return "Public Nuisance Crimes";
                case 65:
                    return "Juvenile Crimes";
                case 66:
                    return "Driving &amp; Traffic Crimes";
                case 67:
                    return "Mugging";
                case 68:
                    return "Other Crimes";
                case 69:
                    return "Bad People Activity";
                case 70:
                    return "Climate Change";
                case 71:
                    return "Early Marriage";
                case 72:
                    return "Religious Violence";
                case 73:
                    return "Fraud";
                case 74:
                    return "Illegal Subscription";
                case 75:
                    return "Adulterant Food and Product";
                case 76:
                    return "Other Social Decadence";
            }
        return "";
    }

    // invert the isSupported boolean and update the support count by +1 or -1,
    // called when user clicks the support button (supports or unsupports a post)
    public void toggleSupport(){
        isSupported = !isSupported;
        countSupport = isSupported ? countSupport+1 : countSupport-1;
    }

    public void incrementCountComment(){
        countComment++;
    }


    public Boolean getIsAnonymous() {
        return isAnonymous;
    }

    public void setIsAnonymous(Boolean isAnonymous) {
        this.isAnonymous = isAnonymous;
    }

    public String getPostImagePrimary() {
        return postImagePrimary;
    }

    public void setPostImagePrimary(String postImagePrimary) {
        this.postImagePrimary = postImagePrimary;
    }

    public int getSupportGoal() {
        return supportGoal;
    }

    public void setSupportGoal(int supportGoal) {
        this.supportGoal = supportGoal;
    }
}
