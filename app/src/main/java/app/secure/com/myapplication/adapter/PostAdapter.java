package app.secure.com.myapplication.adapter;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

import java.util.ArrayList;

import app.secure.com.myapplication.network.TrackSingleton;
import app.secure.com.myapplication.view.CircularNetworkImageView;
import app.secure.com.myapplication.R;
import app.secure.com.myapplication.pojo.Post;
import app.secure.com.myapplication.network.TrackURL;

/**
 * Created by Saimon on 24-Oct-15.
 */
public class PostAdapter extends BaseAdapter {
    private ArrayList<Post> postList;
    private Context context;
    private LayoutInflater inflater = null;
    private ImageLoader imageLoader;

    public PostAdapter(final Context context, ArrayList<Post> postList) {
        this.context = context;
        this.postList = postList;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        imageLoader = TrackSingleton.getInstance(context).getImageLoader();
    }

    @Override
    public Post getItem(int position) {
        return postList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getCount() {
        return postList.size();
    }

    @Override
    public View getView(final int position, View rowView, ViewGroup parent) {
        final Holder holder;
        if (rowView == null) {
            holder = new Holder();
            rowView = inflater.inflate(R.layout.list_item_post, null);
            holder.username = (TextView) rowView.findViewById(R.id.username);
            holder.postType = (TextView) rowView.findViewById(R.id.post_type);
            holder.reportType = (TextView) rowView.findViewById(R.id.report_type);
            holder.postTime = (TextView) rowView.findViewById(R.id.post_time);
            holder.postText = (TextView) rowView.findViewById(R.id.post_text);
            holder.countSupport = (TextView) rowView.findViewById(R.id.count_support);
            holder.countComment = (TextView) rowView.findViewById(R.id.count_comment);
            holder.countShare = (TextView) rowView.findViewById(R.id.count_share);
            holder.userImage = (CircularNetworkImageView) rowView.findViewById(R.id.user_avatar);
            holder.postImagePrimary = (NetworkImageView) rowView.findViewById(R.id.post_image_primary);
            holder.buttonSupport = (ImageButton) rowView.findViewById(R.id.button_support);
            holder.buttonComment = (ImageButton) rowView.findViewById(R.id.button_comment);
            holder.buttonShare = (ImageButton) rowView.findViewById(R.id.button_share);
            holder.userImage.setImageBitmap(BitmapFactory.decodeResource(context.getResources(), R.drawable.avatar_small));
            rowView.setTag(holder);
        } else {
            holder = (Holder) rowView.getTag();
        }
        Post post = postList.get(position);
        holder.username.setText(post.getUsername());
        if (post.getUserProfileImageUrl() != null && post.getUserProfileImageUrl() != "" && post.getUserProfileImageUrl() != "null")
            holder.userImage.setImageUrl(TrackURL.BASE_URL + post.getUserProfileImageUrl(), imageLoader);
        else
            holder.userImage.setImageBitmap(BitmapFactory.decodeResource(context.getResources(), R.drawable.avatar_small));
        holder.username.setTag(post.getUserId());
        holder.userImage.setTag(post.getUserId());

        if (post.getPostImagePrimary() != null)
            holder.postImagePrimary.setImageUrl(TrackURL.BASE_URL + post.getPostImagePrimary(), imageLoader);
        else
            holder.postImagePrimary.setImageBitmap(null);
        holder.postType.setText(postList.get(position).getPostTypeAsString());
        holder.reportType.setText(postList.get(position).getReportTypeAsString());
        holder.postTime.setText(postList.get(position).getPostTime());
        holder.postText.setText(postList.get(position).getPostText());
        // Support Bangla font
        Typeface font = Typeface.createFromAsset(context.getAssets(), "kalpurush.ttf");
        holder.postText.setTypeface(font);

        holder.countSupport.setText(postList.get(position).getCountSupport() + "");
        holder.countComment.setText(postList.get(position).getCountComment() + "");
        holder.countShare.setText(postList.get(position).getCountShare() + "");


        if (postList.get(position).isSupported())
            holder.buttonSupport.setBackground(context.getResources().getDrawable(R.drawable.support_ico_press));
        else
            holder.buttonSupport.setBackground(context.getResources().getDrawable(R.drawable.support_ico));

        holder.buttonSupport.setTag(position);
        holder.buttonComment.setTag(position);
        holder.buttonShare.setTag(position);
        return rowView;
    }

    private class Holder {
        TextView username;
        TextView postType;
        TextView reportType;
        TextView postTime;
        TextView postText;
        CircularNetworkImageView userImage;
        TextView countSupport;
        TextView countComment;
        TextView countShare;
        NetworkImageView postImagePrimary;
        ImageButton buttonSupport;
        ImageButton buttonComment;
        ImageButton buttonShare;
    }
}
