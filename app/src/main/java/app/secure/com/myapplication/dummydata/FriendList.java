package app.secure.com.myapplication.dummydata;

import java.util.ArrayList;

import app.secure.com.myapplication.pojo.Friend;

/**
 * Created by Arafat on 03/12/2015.
 */
public class FriendList {

    public static ArrayList<Friend> getDummyFriend()
    {
        ArrayList<Friend> friendList = new ArrayList<>();
        Friend friend;

        //1
        friend = new Friend();
        friend.setUsername("John Doe");
        friend.setAddress("Walmart,NY");
        friendList.add(friend);


        //2
        friend = new Friend();
        friend.setUsername("Royal Albert");
        friend.setAddress("Queens,Sydney");
        friendList.add(friend);


        //3
        friend = new Friend();
        friend.setUsername("Albert Moses");
        friend.setAddress("Walmart,NY");
        friendList.add(friend);


        //4
        friend = new Friend();
        friend.setUsername("Al Rashid");
        friend.setAddress("Islamabad,Pakistan");
        friendList.add(friend);


        //5
        friend = new Friend();
        friend.setUsername("Ravi Kishan");
        friend.setAddress("New Delhi,India");
        friendList.add(friend);


        //6
        friend = new Friend();
        friend.setUsername("Maragaretta");
        friend.setAddress("Las Vegas");
        friendList.add(friend);


        //7
        friend = new Friend();
        friend.setUsername("Jonathan Swift");
        friend.setAddress("Walmart,NY");
        friendList.add(friend);


        //8
        friend = new Friend();
        friend.setUsername("Tashtin Mutosu");
        friend.setAddress("Zimbabwe");
        friendList.add(friend);


        //9
        friend = new Friend();
        friend.setUsername("Alan Border");
        friend.setAddress("Walmart,NY");
        friendList.add(friend);


        //10
        friend = new Friend();
        friend.setUsername("Cris Martin");
        friend.setAddress("Roulette Valley,UK");
        friendList.add(friend);

        return friendList;

    }





}
