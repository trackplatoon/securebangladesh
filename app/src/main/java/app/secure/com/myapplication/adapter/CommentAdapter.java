package app.secure.com.myapplication.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import app.secure.com.myapplication.R;
import app.secure.com.myapplication.pojo.Comment;

/**
 * Created by Saimon on 03-Jan-16.
 */
public class CommentAdapter extends BaseAdapter {
    private ArrayList<Comment> commentList;
    private Context context;
    private LayoutInflater inflater = null;
    private View.OnClickListener profileImageListener;

    public CommentAdapter(Context context, ArrayList<Comment> commentList) {
        this.context = context;
        this.commentList = commentList;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public Comment getItem(int position) {
        return commentList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getCount() {
        return commentList.size();
    }

    @Override
    public View getView(final int position, View rowView, ViewGroup parent){
        Holder holder;
        if(rowView == null) {
            holder = new Holder();
            rowView = inflater.inflate(R.layout.list_item_comment, null);
            holder.username = (TextView) rowView.findViewById(R.id.username);
            holder.commentText = (TextView) rowView.findViewById(R.id.comment_text);
            rowView.setTag(holder);
        }
        else {
            holder = (Holder) rowView.getTag();
        }
        final Comment comment = commentList.get(position);
        holder.username.setText(comment.getUsername());
        //holder.commentText.setText(comment.getCommentText());
        SpannableString ss = new SpannableString(comment.getUsername() + " " + comment.getCommentText());
        Log.e("Nested", ss.toString());
        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                Toast.makeText(context, "User is:" + comment.getUsername(), Toast.LENGTH_SHORT);
            }
            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
            }
        };
        ss.setSpan(clickableSpan, 0, comment.getUsername().length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        holder.commentText.setText(ss);
        holder.commentText.setMovementMethod(LinkMovementMethod.getInstance());
        holder.commentText.setHighlightColor(Color.TRANSPARENT);
        // TODO: go to profile page on username click
        return rowView;
    }

    private class Holder{
        TextView username;
        TextView commentText;
    }
}
