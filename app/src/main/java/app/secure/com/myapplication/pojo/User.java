package app.secure.com.myapplication.pojo;

import java.io.Serializable;

/**
 * Created by Saimon on 26-Dec-15.
 */
public class User implements Serializable{
    private int userId;
    private String username;
    private String userProfileImageUrl;
    private String userCoverImageUrl;
    private boolean isFollowed;

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUserProfileImageUrl() {
        return userProfileImageUrl;
    }

    public void setUserProfileImageUrl(String userProfileImageUrl) {
        this.userProfileImageUrl = userProfileImageUrl;
    }

    public String getUserCoverImageUrl() {
        return userCoverImageUrl;
    }

    public void setUserCoverImageUrl(String userCoverImageUrl) {
        this.userCoverImageUrl = userCoverImageUrl;
    }

    public boolean isFollowed() {
        return isFollowed;
    }

    public void setIsFollowed(boolean isFollowed) {
        this.isFollowed = isFollowed;
    }

    public void toggleIsFollowed(){
        isFollowed = !isFollowed;
    }
}
