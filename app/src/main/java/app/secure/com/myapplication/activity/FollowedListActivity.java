package app.secure.com.myapplication.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.json.JSONObject;

import java.util.ArrayList;

import app.secure.com.myapplication.R;
import app.secure.com.myapplication.adapter.FollowedUserListAdapter;
import app.secure.com.myapplication.pojo.User;
import app.secure.com.myapplication.pojo.requestprimitive.ResponseObjectFollowedUserList;
import app.secure.com.myapplication.network.NetworkRequestHelper;

public class FollowedListActivity extends DrawerActivity{

    private ListView listViewFollwedUsers;
    private TextView totalFollowCount;
    private LinearLayout mainView;
    private TextView status;

    FollowedUserListAdapter followedUserListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_followed_list);
        super.init();
        listViewFollwedUsers = (ListView) findViewById(R.id.followed_list_view);
        totalFollowCount = (TextView) findViewById(R.id.textview_total_follow_count);
        mainView = (LinearLayout) findViewById(R.id.container);
        status = (TextView) findViewById(R.id.textview_viewfollowed_status);
    }

    @Override
    protected void onResume() {
        super.onResume();
        // call the service for data
        //TrackReceiver mReceiver = new TrackReceiver(new Handler());
        //mReceiver.setListener(this);
        NetworkRequestHelper.startActionGetFollowList(this, createReqViewFollowedListener(), createReqErrorListener());
    }

    // creates and returns a listener that handles the response to view user posts request
    private Response.Listener<JSONObject> createReqViewFollowedListener() {
        return new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    // TODO: fix the conversion from JSON to class
                    ObjectMapper mapper = new ObjectMapper();
                    ResponseObjectFollowedUserList responseBody = mapper.readValue(response.toString(),
                            ResponseObjectFollowedUserList.class);
                    totalFollowCount.setText(responseBody.getCountFollowing() + "");
                    final ArrayList<User> followedUserList = (ArrayList<User>) responseBody.getUsers();
                    followedUserListAdapter = new FollowedUserListAdapter(FollowedListActivity.this, followedUserList);
                    listViewFollwedUsers.setAdapter(followedUserListAdapter);
                    status.setVisibility(View.GONE);
                    mainView.setVisibility(View.VISIBLE);
                    listViewFollwedUsers.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            User user = followedUserList.get(position);
                            if (user != null) {
                                Intent intent = new Intent(FollowedListActivity.this, ViewProfileActivity.class);
                                intent.putExtra("userId", user.getUserId());
                                startActivity(intent);
                            }
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
    }

    private Response.ErrorListener createReqErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        };
    }

    /*
    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
        if (resultCode == 200) {
            ResponseObjectFollowedUserList response = (ResponseObjectFollowedUserList) resultData.get("response_data");
            totalFollowCount.setText(response.getCountFollowing() + "");
            final ArrayList<User> followedUserList = (ArrayList<User>) response.getUsers();
            followedUserListAdapter = new FollowedUserListAdapter(this, followedUserList);
            listViewFollwedUsers.setAdapter(followedUserListAdapter);
            status.setVisibility(View.GONE);
            mainView.setVisibility(View.VISIBLE);
            listViewFollwedUsers.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    User user = followedUserList.get(position);
                    if (user != null) {
                        Intent intent = new Intent(FollowedListActivity.this, ViewProfileActivity.class);
                        intent.putExtra("userId", user.getUserId());
                        startActivity(intent);
                    }
                }
            });
        } else
            status.setText("Server Error: " + resultCode);
    }
    */
}
