package app.secure.com.myapplication.pojo;

import android.graphics.Bitmap;

/**
 * Created by Arafat on 03/12/2015.
 */
public class Friend {

    private String username;
    private String address;
    private Bitmap userImage;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Bitmap getUserImage() {
        return userImage;
    }

    public void setUserImage(Bitmap userImage) {
        this.userImage = userImage;
    }
}
