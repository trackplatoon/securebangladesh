package app.secure.com.myapplication.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.soundcloud.android.crop.Crop;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.IOException;
import java.util.Arrays;

import app.secure.com.myapplication.R;
import app.secure.com.myapplication.fragment.EditProfileFragment;
import app.secure.com.myapplication.global.TrackPlatoon;
import app.secure.com.myapplication.network.FacebookShareHelper;
import app.secure.com.myapplication.network.NetworkRequestHelper;
import app.secure.com.myapplication.pojo.requestprimitive.RequestObjectSignUp;
import app.secure.com.myapplication.pojo.requestprimitive.ResponseObjectLogin;
import app.secure.com.myapplication.pojo.requestprimitive.UserEntity;

public class SignInActivityNew extends AppCompatActivity {

    CallbackManager callbackManager;
    private RelativeLayout containerLogin;
    private LinearLayout containerTerms;
    private LoginButton loginButton;
    private Toolbar toolbar;
    private TextView textViewStatus;
    private EditProfileFragment editProfileFragment;
    private Button startNow;
    private CheckBox checkBox;
    private UserEntity user;
    private LoginManager loginManager;

    private int PICK_IMAGE_REQUEST = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.e("DEBUG", "onCreate");
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_sign_in_activity_new);
        callbackManager = FacebookShareHelper.init(this);
        loginButton = (LoginButton)findViewById(R.id.login_button);
        loginButton.setReadPermissions(Arrays.asList("email"));
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        containerLogin = (RelativeLayout) findViewById(R.id.container_login);
        containerTerms = (LinearLayout) findViewById(R.id.container_terms);

        startNow = (Button) findViewById(R.id.button_start_now);
        checkBox = (CheckBox) findViewById(R.id.checkbox_agreement_to_terms);
        textViewStatus = (TextView) findViewById(R.id.textview_login_status);
        setSupportActionBar(toolbar);
        loginManager = LoginManager.getInstance();
        loginManager.logOut();

        loginButton.registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
            // successfully logged into facebook
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.e("DEBUG", "facebook login success");
                onFacebookLoginSuccess(loginResult);
            }

            @Override
            public void onCancel() {
                Log.e("DEBUG", "facebook login cancelled");
            }

            // Failed to login to facebook
            @Override
            public void onError(FacebookException error) {
                Log.e("DEBUG", "Facebook login error");
                textViewStatus.setText("An error occurred");
                error.printStackTrace();
            }
        });
    };

//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        callbackManager.onActivityResult(requestCode, resultCode, data);
//    }

    protected void onActivityResult(int requestCode, int resultCode, Intent result) {
        super.onActivityResult(requestCode, resultCode, result);
        if(requestCode != PICK_IMAGE_REQUEST && requestCode != Crop.REQUEST_CROP)
            callbackManager.onActivityResult(requestCode, resultCode, result);
        else if(editProfileFragment != null)
            editProfileFragment.onActivityResult(requestCode, resultCode, result);
    }

    private void onFacebookLoginSuccess(LoginResult loginResult){
        Log.e("DEBUG", "onFacebookLoginSuccess");
        loginButton.setVisibility(View.INVISIBLE);
        textViewStatus.setText("Connecting to TrackPlatoon server...");
        Log.e("Access Token", loginResult.getAccessToken().getToken());
        //final FBUser fbUser = new FBUser();
        GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(),
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        try {
                            Log.e("OBJECT: ", object.toString());
                            Log.e("RESPONSE: ", response.toString());
                            Log.e("id", object.getString("id"));
                            String name = object.getString("first_name")
                                    + " " + object.getString("middle_name")
                                    + " " + object.getString("last_name");
                            user = new UserEntity(name, object.getString("email"), object.getString("id"));

                            NetworkRequestHelper.startActionLogin(SignInActivityNew.this,
                                    user,
                                    createLoginReqListener(),
                                    createErrorListener());

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id, first_name, last_name, email,gender, birthday, location,middle_name");
        request.setParameters(parameters);
        request.executeAsync();
    }

    private Response.Listener<String> createLoginReqListener(){
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    ObjectMapper mapper = new ObjectMapper();
                    ResponseObjectLogin responseBody = mapper.readValue(response, ResponseObjectLogin.class);
                    if (responseBody.getAccess_token() == null) {
                        // user is not registered, go through profile setup process.
                        Log.e("AUTH", "SignInActivity: user not registered");
                        containerLogin.setVisibility(View.GONE);
                        toolbar.setVisibility(View.VISIBLE);
                        containerTerms.setVisibility(View.VISIBLE);
                    }else{
                        // user is registered, store the access token and proceed to newsfeed.
                        Log.e("AUTH", "SignInActivity: user registered");
                        // set user id = 0 because the idiot who coded the backend can't provide it along with the access token.
                        // retrieve user id on the next request to server, along with newsfeed data
                        ((TrackPlatoon) getApplication()).setCredentials(0, responseBody.getAccess_token());
                        Intent intent = new Intent(SignInActivityNew.this, HomeActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };
    }

    private Response.Listener<String> createSignUpReqListener(){
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    ObjectMapper mapper = new ObjectMapper();
                    ResponseObjectLogin responseBody = mapper.readValue(response, ResponseObjectLogin.class);
                    if (responseBody.getAccess_token() == null) {
                        // TODO: server error, handle
                        Log.e("AUTH", "ProfilePageActivity: could not retrieve access token from server");
                    }else{
                        // logged in successfully
                        Log.e("AUTH", "ProfilePageSetup: user logged in successfully");
                        ((TrackPlatoon) getApplication()).setCredentials(0, responseBody.getAccess_token());
                        Intent intent = new Intent(SignInActivityNew.this, HomeActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish();
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };
    }

    private Response.ErrorListener createErrorListener(){
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(SignInActivityNew.this, "Server error: " + error.toString(), Toast.LENGTH_SHORT).show();
                loginManager.logOut();
                textViewStatus.setVisibility(View.GONE);
                loginButton.setVisibility(View.VISIBLE);

            }
        };
    }

    //onClick methods
    public void toggleAgreement(View view){
        boolean checked = ((CheckBox) view).isChecked();
        if(checked) {
            startNow.setEnabled(true);
        }
        else{
            startNow.setEnabled(false);
        }
    }

    public void startNow(View view){
        editProfileFragment = EditProfileFragment.newInstance();
        containerTerms.setVisibility(View.GONE);
        getFragmentManager().beginTransaction().add(R.id.container, editProfileFragment).commit();
    }

    public void pickProfilePhotoFromPhone(View view) {
        editProfileFragment.showFileChooser();
    }

    public void onSubmitUserDetails(View view) {
        RequestObjectSignUp requestObject = editProfileFragment.collectData();
        requestObject.setEmail(user.getEmail());
        requestObject.setPassword(user.getPassword());
        requestObject.setUsername(user.getName());
        Log.e("READY EMAIL", user.getEmail());
        Log.e("READY NAME", user.getName());
        Log.e("READY PASS", user.getPassword());
        NetworkRequestHelper.startActionSignUp(SignInActivityNew.this,
                requestObject,
                createSignUpReqListener(),
                createErrorListener());
    }

    // image crop methods
    private void beginCrop(Uri source) {
        if(editProfileFragment != null)
            editProfileFragment.beginCrop(source);
    }

    private void handleCrop(int resultCode, Intent result){
        if(editProfileFragment != null)
            editProfileFragment.handleCrop(resultCode, result);
    }

}
