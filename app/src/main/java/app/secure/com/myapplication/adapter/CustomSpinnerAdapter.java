package app.secure.com.myapplication.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import app.secure.com.myapplication.R;

/**
 * Created by Saimon on 30-Nov-15.
 */
public class CustomSpinnerAdapter extends ArrayAdapter<String> {
    Context context;
    String[] objects;
    public CustomSpinnerAdapter(Context context, int txtViewResourceId, String[] objects) {
        super(context, txtViewResourceId, objects);
        this.context = context;
        this.objects = objects;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getCustomDropDownView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    public View getCustomView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = ((Activity) context).getLayoutInflater();
        View view = inflater.inflate(R.layout.custom_spinner, parent, false);
        TextView main_text = (TextView) view.findViewById(R.id.text_spinner_selected);
        main_text.setText(objects[position]);
        return view;
    }

    public View getCustomDropDownView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = ((Activity) context).getLayoutInflater();
        View view = inflater.inflate(R.layout.custom_spinner, parent, false);
        TextView main_text = (TextView) view.findViewById(R.id.text_spinner_selected);
        ImageButton ico = (ImageButton) view.findViewById(R.id.spinner_expand);
        ico.setVisibility(View.GONE);
        main_text.setText(objects[position]);
        return view;
    }
}
