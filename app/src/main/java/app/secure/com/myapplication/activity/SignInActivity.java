package app.secure.com.myapplication.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import app.secure.com.myapplication.R;
import app.secure.com.myapplication.global.TrackPlatoon;
import app.secure.com.myapplication.pojo.requestprimitive.ResponseObjectLogin;
import app.secure.com.myapplication.pojo.requestprimitive.UserEntity;
import app.secure.com.myapplication.network.NetworkRequestHelper;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Arrays;


public class SignInActivity extends AppCompatActivity {
    private TextView textViewStatus;
    LoginButton loginButton;
    private UserEntity user;

    CallbackManager callbackManager;
    private AccessTokenTracker accessTokenTracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //initializing the sdk
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_sign_in);

        callbackManager = CallbackManager.Factory.create();

        loginButton = (LoginButton) findViewById(R.id.login_button);
        textViewStatus = (TextView) findViewById(R.id.textview_login_status);
        loginButton.setReadPermissions(Arrays.asList("email"));
        //registering callback
        loginButton.registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        loginButton.setVisibility(View.INVISIBLE);
                        textViewStatus.setText("Connecting to TrackPlatoon server...");
                        Log.e("Access Token", loginResult.getAccessToken().getToken());
                        //final FBUser fbUser = new FBUser();
                        GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(),
                                new GraphRequest.GraphJSONObjectCallback() {
                                    @Override
                                    public void onCompleted(JSONObject object, GraphResponse response) {
                                        try {
                                            Log.e("OBJECT: ", object.toString());
                                            Log.e("RESPONSE: ", response.toString());
                                            Log.e("id", object.getString("id"));
                                            String name = object.getString("first_name")
                                                    + " " + object.getString("middle_name")
                                                    + " " + object.getString("last_name");
                                            user = new UserEntity(name, object.getString("email"), object.getString("id"));

                                            NetworkRequestHelper.startActionLogin(SignInActivity.this,
                                                    user,
                                                    createLoginReqListener(),
                                                    createErrorListener());

                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }

                                });
                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "id, first_name, last_name, email,gender, birthday, location,middle_name");
                        request.setParameters(parameters);
                        request.executeAsync();
                    }

                    @Override
                    public void onCancel() {
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        textViewStatus.setText("An error occurred");
                        Log.e("Error", "Error");
                        exception.printStackTrace();
                    }
                });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private Response.Listener<String> createLoginReqListener() {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    ObjectMapper mapper = new ObjectMapper();
                    ResponseObjectLogin responseBody = mapper.readValue(response, ResponseObjectLogin.class);
                    if (responseBody.getAccess_token() == null) {
                        // user is not registered, go through profile setup process.
                        Log.e("AUTH", "SignInActivity: user not registered");
                        Intent intent = new Intent(SignInActivity.this, TermsAndCondition.class);
                        // propagate the user entity object forward.
                        intent.putExtra("FACEBOOK_USER", user);
                        startActivity(intent);
                        finish();
                    } else {
                        // user is registered, store the access token and proceed to newsfeed.
                        Log.e("AUTH", "SignInActivity: user registered");
                        // set user id = 0 because the idiot who coded the backend can't provide it along with the access token.
                        // retrieve user id on the next request to server, along with newsfeed data
                        ((TrackPlatoon) getApplication()).setCredentials(0, responseBody.getAccess_token());
                        Intent intent = new Intent(SignInActivity.this, HomeActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish();
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };
    }

    private Response.ErrorListener createErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(SignInActivity.this, "Server error: " + error.toString(), Toast.LENGTH_SHORT).show();
            }
        };
    }
}
