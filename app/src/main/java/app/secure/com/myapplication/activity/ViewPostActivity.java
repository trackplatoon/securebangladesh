package app.secure.com.myapplication.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.NetworkImageView;
import com.facebook.CallbackManager;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.json.JSONObject;

import java.util.ArrayList;

import app.secure.com.myapplication.R;
import app.secure.com.myapplication.adapter.CommentAdapter;
import app.secure.com.myapplication.global.TrackPlatoon;
import app.secure.com.myapplication.pojo.Comment;
import app.secure.com.myapplication.pojo.Post;
import app.secure.com.myapplication.pojo.requestprimitive.ResponseObjectViewPost;
import app.secure.com.myapplication.network.NetworkRequestHelper;
import app.secure.com.myapplication.network.TrackSingleton;
import app.secure.com.myapplication.view.CircularNetworkImageView;
import app.secure.com.myapplication.network.FacebookShareHelper;
import app.secure.com.myapplication.network.TrackURL;

public class ViewPostActivity extends DrawerActivity{

    private int postId;
    private Post post;
    private LinearLayout mainView;
    private CircularNetworkImageView userImage;
    private TextView status;
    private TextView username;
    private TextView postType;
    private TextView reportType;
    private TextView postTime;
    private TextView postText;
    private TextView countSupport;
    private TextView countShare;
    private NetworkImageView postImagePrimary;
    private LinearLayout commentLayout;
    private CommentAdapter mAdapter;
    private ArrayList<Comment> comments;
    private ImageButton postCommentButton;
    private ImageButton postSupportButton;
    private EditText newCommentText;
    private CallbackManager callbackManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_viewpost);
        // Sets up the drawer and toolbar
        // Call this method after calling setContentView()
        super.init();

        mainView = (LinearLayout) findViewById(R.id.container);
        status = (TextView) findViewById(R.id.textview_viewpost_status);
        userImage = (CircularNetworkImageView) findViewById(R.id.user_avatar);
        username = (TextView) findViewById(R.id.username);
        postType = (TextView) findViewById(R.id.post_type);
        reportType = (TextView) findViewById(R.id.report_type);
        postText = (TextView) findViewById(R.id.post_text);
        postTime = (TextView) findViewById(R.id.post_time);
        countSupport = (TextView) findViewById(R.id.count_support_viewpost);
        countShare = (TextView) findViewById(R.id.count_share_viewpost);
        postImagePrimary = (NetworkImageView) findViewById(R.id.post_image_primary);
        postSupportButton = (ImageButton) findViewById(R.id.button_support_viewpost);
        postCommentButton = (ImageButton) findViewById(R.id.button_comment);
        newCommentText = (EditText) findViewById(R.id.comment_text);
        commentLayout = (LinearLayout) findViewById(R.id.layout_comments);
        userImage.setImageBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.avatar_small));
    }

    @Override
    protected void onResume() {
        super.onResume();
        // call the service for data
        Intent intent = getIntent();
        this.postId = intent.getIntExtra("POST_ID", 0);
        this.post = (Post) intent.getSerializableExtra("POST");
        NetworkRequestHelper.startActionViewPost(this,
                postId,
                createReqViewPostListener(),
                createReqErrorListener());
    }

    // creates and returns a listener that handles the response to view post request
    private Response.Listener<JSONObject> createReqViewPostListener() {
        return new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    // TODO: fix the conversion from JSON to class
                    ObjectMapper mapper = new ObjectMapper();
                    ResponseObjectViewPost responseBody = mapper.readValue(response.toString(), ResponseObjectViewPost.class);
                    post = responseBody.getData();
                    username.setText(post.getUsername());
                    username.setTag(post.getUserId());
                    postType.setText(post.getPostTypeAsString());
                    reportType.setText(post.getReportTypeAsString());
                    postTime.setText(post.getPostTime());
                    postText.setText(post.getPostText());
                    countSupport.setText(post.getCountSupport() + "");
                    countShare.setText(post.getCountShare() + "");
                    // set button status
                    if (post.isSupported())
                        postSupportButton.setBackground(getResources().getDrawable(R.drawable.support_ico_press));
                    else
                        postSupportButton.setBackground(getResources().getDrawable(R.drawable.support_ico));

                    if(post.getUserProfileImageUrl() != null)
                        userImage.setImageUrl(TrackURL.BASE_URL +post.getUserProfileImageUrl(),
                                TrackSingleton.getInstance(ViewPostActivity.this).getImageLoader());
                    if(post.getPostImagePrimary() != null)
                        postImagePrimary.setImageUrl(TrackURL.BASE_URL + post.getPostImagePrimary(),
                                TrackSingleton.getInstance(ViewPostActivity.this).getImageLoader());
                    // use the tag to launch ViewProfileActivity
                    userImage.setTag(post.getUserId());
                    username.setTag(post.getUserId());

                    comments = (ArrayList) responseBody.getComments();
                    // clear layout before adding the comments to prevent duplicates
                    commentLayout.removeAllViews();
                    // display comments by iterating through ArrayList and adding each to LinearLayout
                    for (Comment comment : comments) {
                        View view = getLayoutInflater().inflate(R.layout.list_item_comment, null);
                        LinearLayout layoutSupportComment = (LinearLayout) view.findViewById(R.id.layout_support_comment);
                        TextView commentUsername = (TextView) view.findViewById(R.id.comment_username);
                        TextView commentText = (TextView) view.findViewById(R.id.comment_text);
                        TextView commentCountSupport = (TextView) view.findViewById(R.id.count_comment_support);
                        ImageButton supportButton = (ImageButton) view.findViewById(R.id.button_support_comment);
                        if (comment.getIsSupported())
                            supportButton.setBackground(getResources().getDrawable(R.drawable.support_ico_press));
                        // set the comment object as tag so it can be retrieved on click events
                        layoutSupportComment.setTag(R.string.tag_comment_index, comments.indexOf(comment));
                        commentUsername.setText(comment.getUsername());
                        commentText.setText(comment.getCommentText());
                        commentCountSupport.setText(comment.getCountSupport() + "");
                        // use the tag to launch ViewProfileActivity
                        commentUsername.setTag(comment.getUserId());
                        commentLayout.addView(view);
                    }
                    // remove the placeholder and make the main layout visible
                    status.setVisibility(View.GONE);
                    mainView.setVisibility(View.VISIBLE);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
    }

    // creates and returns a listener that handles the response to new comment request
    private Response.Listener<JSONObject> createReqNewCommentListener() {
        return new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    newCommentText.setText("");
                    postCommentButton.setEnabled(true);
                    Toast.makeText(ViewPostActivity.this, "Posted comment!", Toast.LENGTH_SHORT).show();
                    NetworkRequestHelper.startActionViewPost(ViewPostActivity.this,
                            postId,
                            createReqViewPostListener(),
                            createReqErrorListener());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
    }

    private Response.ErrorListener createReqErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        };
    }

    public void usernameClick(View v){
        Log.e("View user", "Call to usernameClick()");
        int userId = 0;
        try {
            userId = (Integer) v.getTag();
        }catch (NullPointerException e){ Log.e("OOPS", "Null username"); return; }
        Log.e("View user", "userId = " + userId);
        if (userId != 0) {
            // go to my own profile
            if(userId == ((TrackPlatoon) getApplication()).getCredentials().getUserId())
                startActivity(new Intent(ViewPostActivity.this, ProfileActivity.class));
                // go to another user profile other than my own
            else {
                Intent intent = new Intent(this, ViewProfileActivity.class);
                intent.putExtra("userId", userId);
                startActivity(intent);
            }
        }
    }

    public void supportPostClick(View v) {
        if (post != null) {
            post.toggleSupport();
            if (post.isSupported())
                postSupportButton.setBackground(getResources().getDrawable(R.drawable.support_ico_press));
            else
                postSupportButton.setBackground(getResources().getDrawable(R.drawable.support_ico));
            countSupport.setText(post.getCountSupport() + "");
            NetworkRequestHelper.startActionSupportPost(this, post.getPostId(), post.isSupported());
        }
    }

    // update the status of the support button and the support count
    public void supportCommentClick(View v) {
        Comment comment = comments.get((int) v.getTag(R.string.tag_comment_index));
        if (comment != null) {
            comment.toggleSupport();
            // nasty bit of code here
            // click event is fired from the parent view
            // iterate through child views to update them
            try {
                for(int i=0 ; i<((ViewGroup)v).getChildCount() ; ++i) {
                    View nextChild = ((ViewGroup)v).getChildAt(i);
                    if(nextChild instanceof TextView){
                        ((TextView) nextChild).setText(comment.getCountSupport() + "");
                    }else if(nextChild instanceof ImageButton){
                        if (comment.getIsSupported())
                            nextChild.setBackground(getResources().getDrawable(R.drawable.support_ico_press));
                        else
                            nextChild.setBackground(getResources().getDrawable(R.drawable.support_ico));
                    }
                }
            }catch (Exception e){ e.printStackTrace(); }
            NetworkRequestHelper.startActionSupportComment(this, comment.getCommentId(), comment.getIsSupported());
        }
    }

    public void postCommentClick(View v) {
        // allow posting the comment only if the string is not entirely composed of whitespaces
        if (newCommentText.getText().toString().replaceAll("\\s+", "") != "") {
            v.setEnabled(false);
            Comment comment = new Comment();
            comment.setCommentText(newCommentText.getText().toString().trim());    // remove leading and trailing spaces
            NetworkRequestHelper.startActionNewComment(ViewPostActivity.this,
                    postId,
                    comment,
                    createReqNewCommentListener(),
                    createReqErrorListener());
        }
    }

    public void sharePostClick(View v) {
        if(callbackManager == null)
            callbackManager = FacebookShareHelper.init(this);
        FacebookShareHelper.share(this,
                post,
                "http://www.trackplatoon.com");
    }


    // facebook share confirmation
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
        if(resultCode == Activity.RESULT_OK){
            Log.e("RESULT CODEs", Integer.toString(resultCode));
            NetworkRequestHelper.startActionSharePost(this, post.getPostId());
        }
        else {
            Log.e("RESULT ", Integer.toString(resultCode));
        }

    }

}
