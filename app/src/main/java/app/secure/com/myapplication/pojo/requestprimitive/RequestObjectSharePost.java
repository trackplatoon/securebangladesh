package app.secure.com.myapplication.pojo.requestprimitive;

/**
 * Created by Saimon on 29-Jan-16.
 */
public class RequestObjectSharePost {
    private int postId;

    public RequestObjectSharePost(int postId){
        setPostId(postId);
    }

    public int getPostId() {
        return postId;
    }

    public void setPostId(int postId) {
        this.postId = postId;
    }
}
