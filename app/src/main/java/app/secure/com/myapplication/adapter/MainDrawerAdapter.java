package app.secure.com.myapplication.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;

import java.util.List;

import app.secure.com.myapplication.R;
import app.secure.com.myapplication.network.TrackSingleton;
import app.secure.com.myapplication.network.TrackURL;
import app.secure.com.myapplication.pojo.DrawerItem;
import app.secure.com.myapplication.view.CircularNetworkImageView;

/**
 * Created by Saimon on 31-Oct-15.
 */
public class MainDrawerAdapter extends ArrayAdapter<DrawerItem> {

    Context context;
    List<DrawerItem> drawerItemList;
    int layoutResID;
    private ImageLoader imageLoader;

    public MainDrawerAdapter(Context context, int layoutResourceID,
                             List<DrawerItem> listItems) {
        super(context, layoutResourceID, listItems);
        this.context = context;
        this.drawerItemList = listItems;
        this.layoutResID = layoutResourceID;
        imageLoader = TrackSingleton.getInstance(context).getImageLoader();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub

        DrawerItemHolder drawerHolder;
        View view = convertView;

        if (view == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            drawerHolder = new DrawerItemHolder();
            view = inflater.inflate(layoutResID, parent, false);
            drawerHolder.profileImage = (CircularNetworkImageView) view.findViewById(R.id.drawer_profile_image);
            drawerHolder.name = (TextView) view.findViewById(R.id.drawer_name);
            drawerHolder.icon = (ImageView) view.findViewById(R.id.drawer_icon);
            drawerHolder.notificationCount = (TextView) view.findViewById(R.id.drawer_notification_count);

            view.setTag(drawerHolder);

        } else {
            drawerHolder = (DrawerItemHolder) view.getTag();
        }

        DrawerItem dItem = this.drawerItemList.get(position);
        if(dItem.getIconResID() == 0) {
            if(dItem.getUserProfileImageUrl() != null) {
                drawerHolder.profileImage.setImageUrl(TrackURL.BASE_URL + dItem.getUserProfileImageUrl(), imageLoader);
                drawerHolder.notificationCount.setBackground(null);
            }else{
                drawerHolder.profileImage.setImageBitmap(BitmapFactory
                        .decodeResource(context.getResources(), R.drawable.avatar_small));
            }
        }
        else {
            Log.e("DRAWER", "Unread notifications: "+dItem.getNotificationCount());
            drawerHolder.icon.setImageDrawable(view.getResources().getDrawable(dItem.getIconResID()));
            drawerHolder.name.setText(dItem.getName());
            if(dItem.getNotificationCount() != 0){
                drawerHolder.notificationCount.setText(dItem.getNotificationCount() + "");
                drawerHolder.notificationCount.setVisibility(View.VISIBLE);
            }
            else {
                drawerHolder.notificationCount.setVisibility(View.INVISIBLE);
            }
        }
        return view;
    }

    private class DrawerItemHolder {
        TextView name;
        ImageView icon;
        CircularNetworkImageView profileImage;
        TextView notificationCount;
    }
}
