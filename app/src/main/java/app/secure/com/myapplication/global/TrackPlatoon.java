package app.secure.com.myapplication.global;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import app.secure.com.myapplication.R;

/**
 * Created by Saimon on 26-Dec-15.
 */
public class TrackPlatoon extends Application {

    public static final String PREFS_NAME = "tp_shared_pref";
    private Credentials credentials;

    // on initialization looks for authentication saved in sharedPref
    @Override
    public void onCreate(){
        super.onCreate();
        credentials = new Credentials();
        SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        this.credentials.setUserId(sharedPref.getInt(getString(R.string.auth_userid), 0));
        this.credentials.setToken(sharedPref.getString(getString(R.string.auth_token), null));
        Log.e("SAVED TOKEN", this.credentials.getToken() + "");
    }

    // update authentication info
    public void setCredentials(int userId, String token){
        // save to sharedPref
        SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt(getString(R.string.auth_userid), userId);
        editor.putString(getString(R.string.auth_token), token);
        editor.commit();
        // update credential object
        this.credentials.setUserId(userId);
        this.credentials.setToken(token);
    }

    public Credentials getCredentials(){
        return this.credentials;
    }
}

