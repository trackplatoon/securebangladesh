package app.secure.com.myapplication.pojo.requestprimitive;

import java.io.Serializable;
import java.util.ArrayList;

import app.secure.com.myapplication.pojo.Notification;

/**
 * Created by Saimon on 16-Jan-16.
 */
public class ResponseObjectNotifications implements Serializable{
    private boolean success;
    private int countUnread;
    private ArrayList<Notification> notifications;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public ArrayList<Notification> getNotifications() {
        return notifications;
    }

    public void setNotifications(ArrayList<Notification> notifications) {
        this.notifications = notifications;
    }

    public int getCountUnread() {
        return countUnread;
    }

    public void setCountUnread(int countUnread) {
        this.countUnread = countUnread;
    }
}
