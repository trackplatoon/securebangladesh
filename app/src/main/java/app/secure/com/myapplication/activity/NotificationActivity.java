package app.secure.com.myapplication.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.json.JSONObject;

import java.util.ArrayList;

import app.secure.com.myapplication.R;
import app.secure.com.myapplication.adapter.NotificationAdapater;
import app.secure.com.myapplication.pojo.Notification;
import app.secure.com.myapplication.pojo.requestprimitive.ResponseObjectNotifications;
import app.secure.com.myapplication.network.NetworkRequestHelper;

public class NotificationActivity extends DrawerActivity{

    private ArrayList<Notification> notifications;
    private ListView notificaListView;
    private NotificationAdapater notificationAdapater;
    private LinearLayout mainView;
    private TextView status;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        super.init();
        notificaListView = (ListView) findViewById(R.id.notification_list_view);
        mainView = (LinearLayout) findViewById(R.id.container);
        status = (TextView) findViewById(R.id.textview_viewfollowed_status);

    }

    protected void onResume() {
        super.onResume();
        NetworkRequestHelper.startActionGetNotifications(this, createNotificationReqListener(), createReqErrorListener());
    }

    // creates and returns a listener that handles the response to notification request
    private Response.Listener<JSONObject> createNotificationReqListener() {
        return new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    // TODO: fix the conversion from JSON to class
                    ObjectMapper mapper = new ObjectMapper();
                    ResponseObjectNotifications responseBody = responseBody = mapper.readValue(response.toString(),
                            ResponseObjectNotifications.class);
                            notifications = responseBody.getNotifications();
                    if( notifications.isEmpty()){
                        status.setText("You have no notifications");
                        return;
                    }
                    notificationAdapater = new NotificationAdapater(NotificationActivity.this, notifications);
                    notificaListView.setAdapter(notificationAdapater);
                    status.setVisibility(View.GONE);
                    mainView.setVisibility(View.VISIBLE);
                    notificaListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            Log.e("DEBUG", "Handling notification click: " +position);
                            Intent intent;
                            Notification notification = notifications.get(position);
                            if (notification != null) {
                                switch (notification.getType()) {
                                    case "follow":
                                        intent = new Intent(NotificationActivity.this, ViewProfileActivity.class);
                                        intent.putExtra("userId", notification.getSenderId());
                                        startActivity(intent);
                                        break;
                                    case "comment":
                                    case "support":
                                    case "support_comment":
                                        intent = new Intent(NotificationActivity.this, ViewPostActivity.class);
                                        intent.putExtra("POST_ID", notification.getPostId());
                                        startActivity(intent);
                                        break;
                                    default:
                                        break;
                                }
                                if (!notification.getIsRead())
                                    NetworkRequestHelper.startActionReadNotification(NotificationActivity.this,
                                            notification.getNotificationId());
                            }
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
    }

    private Response.ErrorListener createReqErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                status.setText("Server Error: " + error.getMessage());
            }
        };
    }
}
