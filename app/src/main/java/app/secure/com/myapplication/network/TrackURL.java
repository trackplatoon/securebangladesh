package app.secure.com.myapplication.network;

/**
 * Created by Saimon on 05-Jan-16.
 */
public class TrackURL {
    public static final String BASE_URL = "http://trackplatoonapp.com/api/laravel/public/";
    //public static final String BASE_URL = " http://192.168.0.100:8000/";
    public static final String URL_DASHBOARD = BASE_URL + "dashboard";
    public static final String URL_FEED = BASE_URL + "posts/all";
    public static final String URL_DISCOVER_PEOPLE = BASE_URL + "discover";
    public static final String URL_SIGNUP = BASE_URL + "register";
    public static final String URL_NOTIFICATION = BASE_URL + "notifications";
    public static final String URL_GETUSERDETAILS = BASE_URL + "userinfo";
    public static final String URL_UPDATEUSERDETAILS = BASE_URL + "userinfo";
    public static final String URL_READNOTIFICATION = BASE_URL + "notifications/read";
    public static final String URL_VIEWFOLLOWING = BASE_URL + "follow";
    public static final String URL_NEWPOST = BASE_URL + "posts";
    public static final String URL_VIEWPOST = BASE_URL + "posts/";
    public static final String URL_NEWCOMMENT = BASE_URL + "comment";
    public static final String URL_VIEWUSER = BASE_URL + "user/";
    public static final String URL_VIEWUSERPOSTS = BASE_URL + "user/";
    public static final String URL_SUPPORTPOST = BASE_URL + "supportpost";
    public static final String URL_SUPPORTCOMMENT = BASE_URL + "supportcomment";
    public static final String URL_SHAREPOST = BASE_URL + "share/";
    public static final String URL_FOLLOWUSER = BASE_URL + "follow";
    public static final String URL_LOGIN = BASE_URL + "login";

    //Keys and Secrets for Login Authentication
    public static  final String CLIENT_ID = "f3d259ddd3ed8ff3843839b" ;
    public static  final String CLIENT_SECRET = "4c7f6f8fa93d59c45502c0ae8c4a95b" ;
}
