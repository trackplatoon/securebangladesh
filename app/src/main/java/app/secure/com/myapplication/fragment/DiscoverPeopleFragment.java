package app.secure.com.myapplication.fragment;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.json.JSONObject;

import java.util.ArrayList;

import app.secure.com.myapplication.R;
import app.secure.com.myapplication.adapter.CustomSpinnerAdapter;
import app.secure.com.myapplication.adapter.PeopleAdapter;
import app.secure.com.myapplication.network.NetworkRequestHelper;
import app.secure.com.myapplication.pojo.User;
import app.secure.com.myapplication.pojo.requestprimitive.ResponseObjectDiscoverPeople;

/**
 * Created by Saimon on 16-Oct-15.
 */
public class DiscoverPeopleFragment extends Fragment {
    // Resources should be accessed from values, but context issues must be resolved in order to do that.
    //private String[] objects = getContext().getResources().getStringArray(R.array.aroundme_filter_options);
    private String[] objects = {"People", "Places"};
    private Spinner spinner;
    private GridView gridView;
    private PeopleAdapter peopleAdapter;
    private ArrayList<User> people;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_discoverpeople, container, false);
        spinner = (Spinner) rootView.findViewById(R.id.spinner_aroundme);
        gridView = (GridView) rootView.findViewById(R.id.gridview_aroundme);
        people = new ArrayList<>();
        //peopleAdapter = new PeopleAdapter(getContext(), people);
        //gridView.setAdapter(peopleAdapter);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(getContext(), "Not supported yet", Toast.LENGTH_SHORT).show();
            }
        });
        spinner.setAdapter(new CustomSpinnerAdapter(getActivity(), R.id.text_spinner_selected, objects));
        try {
            NetworkRequestHelper.startActionDiscoverPeople(getContext(), 1, createDiscoverReqListener(), createReqErrorListener());
        }catch (NullPointerException e){e.printStackTrace();}
        return rootView;
    }

    // creates and returns a listener that handles the response to notification request
    private Response.Listener<JSONObject> createDiscoverReqListener() {
        return new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    // TODO: fix the conversion from JSON to class
                    ObjectMapper mapper = new ObjectMapper();
                    ResponseObjectDiscoverPeople responseBody = mapper.readValue(response.toString(),
                            ResponseObjectDiscoverPeople.class);
                    if(responseBody.isSuccess()){
                        people.addAll(responseBody.getPeople());
                        if(peopleAdapter == null){
                            peopleAdapter = new PeopleAdapter(getContext(), people);
                            //gridView.setAdapter(peopleAdapter);
                        }
                        peopleAdapter.notifyDataSetChanged();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
    }

    private Response.ErrorListener createReqErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getActivity(), "Server Error: " + error.getMessage(), Toast.LENGTH_SHORT);
            }
        };
    }
}
