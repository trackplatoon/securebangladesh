package app.secure.com.myapplication.activity;

import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import app.secure.com.myapplication.R;
import app.secure.com.myapplication.global.TrackPlatoon;
import app.secure.com.myapplication.pojo.requestprimitive.RequestObjectSignUp;
import app.secure.com.myapplication.pojo.requestprimitive.ResponseObjectLogin;
import app.secure.com.myapplication.pojo.requestprimitive.UserEntity;
import app.secure.com.myapplication.network.NetworkRequestHelper;
import app.secure.com.myapplication.view.BitmapHelper;
import app.secure.com.myapplication.view.CircularNetworkImageView;

public class ProfilePageSetup extends ActionBarActivity{

    private Toolbar toolbar;
    private Button startNow,takeFromMobile;
    private CircularNetworkImageView profileImageView;
    private ImageView coverImageView;
    private UserEntity user;
    Bitmap bitmap;
    private int PICK_IMAGE_REQUEST = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_page_setup);
        // retrieved from calling activity
        user = (UserEntity) getIntent().getSerializableExtra("FACEBOOK_USER");
        takeFromMobile = (Button)findViewById(R.id.button_take_from_mobile);
        startNow = (Button)findViewById(R.id.button_start_now);
        profileImageView = (CircularNetworkImageView) findViewById(R.id.img_view_pro_pic);
        coverImageView = (ImageView) findViewById(R.id.img_view_cover_pic);
        toolbar = (Toolbar) findViewById(R.id.toolbar_profile_setup);

        if (toolbar != null) {
            toolbar.inflateMenu(R.menu.menu_profile_page_setup);
        }
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        getSupportActionBar().setCustomView(R.layout.custom_profile_setup);

    }

    public void onSubmitUserDetails(View view) {
        if(user != null){
            Log.e("READY EMAIL", user.getEmail());
            Log.e("READY NAME", user.getName());
            Log.e("READY PASS", user.getPassword());
            RequestObjectSignUp requestObject = new RequestObjectSignUp();
            requestObject.setEmail(user.getEmail());
            requestObject.setPassword(user.getPassword());
            requestObject.setUsername(user.getName());
            requestObject.setProfileImage(getStringImage(bitmap));

            NetworkRequestHelper.startActionSignUp(ProfilePageSetup.this,
                    requestObject,
                    createSignUpReqListener(),
                    createErrorListener());
        }
        else
            Log.e("READY FAIL", "Nullified along the way");
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_profile_page_setup, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void pickProfilePhotoFromPhone(View view) {
        showFileChooser();
    }

    // asks the user to pick an application for choosing an image
    private void showFileChooser() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }

    // TODO: use the helper class
    // converts Bitmap image to Base64 encoded image
    public String getStringImage(Bitmap bmp){
        if( bmp != null ) {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            byte[] imageBytes = baos.toByteArray();
            String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
            return encodedImage;
        }
        return null;
    }


    // called after user selects an image
    // set the image in the ImageView
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            Uri filePath = data.getData();
            try {
                AssetFileDescriptor fileDescriptor =
                        this.getContentResolver().openAssetFileDescriptor( filePath, "r");
                //Getting the Bitmap from Gallery
                // Scale it down before loading the actual bitmap
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeFileDescriptor(fileDescriptor.getFileDescriptor(), null, options);
                options.inSampleSize = BitmapHelper.calculateInSampleSize(options, 100, 100);

                // Decode bitmap with inSampleSize set
                options.inJustDecodeBounds = false;

                // load the scaled down version
                bitmap =
                        BitmapFactory.decodeFileDescriptor(fileDescriptor.getFileDescriptor(), null, options);
                //bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                //Setting the Bitmap to ImageView
                profileImageView.setImageBitmap(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private Response.Listener<String> createSignUpReqListener(){
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    ObjectMapper mapper = new ObjectMapper();
                    ResponseObjectLogin responseBody = mapper.readValue(response, ResponseObjectLogin.class);
                    if (responseBody.getAccess_token() == null) {
                        // TODO: server error, handle
                        Log.e("AUTH", "ProfilePageActivity: could not retrieve access token from server");
                    }else{
                        // logged in successfully
                        Log.e("AUTH", "ProfilePageSetup: user logged in successfully");
                        ((TrackPlatoon) getApplication()).setCredentials(0, responseBody.getAccess_token());
                        Intent intent = new Intent(ProfilePageSetup.this, HomeActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish();
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };
    }

    private Response.ErrorListener createErrorListener(){
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(ProfilePageSetup.this, "Error: " + error.toString(), Toast.LENGTH_SHORT).show();
            }
        };
    }
}
