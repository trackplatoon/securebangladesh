package app.secure.com.myapplication.listener;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;

import app.secure.com.myapplication.activity.ProfileActivity;
import app.secure.com.myapplication.activity.ViewProfileActivity;
import app.secure.com.myapplication.global.TrackPlatoon;

/**
 * Created by Saimon on 24-Mar-16.
 */
public class UsernameClickListener implements OnClickListener {
    private Context context;

    public UsernameClickListener(Context context) {
        this.context = context;
    }

    @Override
    public void onClick(View v) {
        Log.e("View user", "Call to usernameClick()");
        int userId = 0;
        try {
            userId = (Integer) v.getTag();
        } catch (NullPointerException e) {
            Log.e("OOPS", "Null username");
            return;
        }
        Log.e("View user", "userId = " + userId);
        if (userId != 0) {
            // go to my own profile
            if (userId == ((TrackPlatoon) context.getApplicationContext()).getCredentials().getUserId()) {
                Log.e("DEBUG", "Going to own profile, userId: " + userId);
                context.startActivity(new Intent(context, ProfileActivity.class));
            }
            // go to another user profile other than my own
            else {
                Log.e("DEBUG", "Going to some other profile, userId: " + userId);
                Intent intent = new Intent(context, ViewProfileActivity.class);
                intent.putExtra("userId", userId);
                context.startActivity(intent);
            }
        }
    }
}
