package app.secure.com.myapplication.pojo.requestprimitive;

import java.io.Serializable;

import app.secure.com.myapplication.pojo.User;

/**
 * Created by Saimon on 15-Jan-16.
 */
public class ResponseObjectViewUser implements Serializable{
    private boolean success;
    private int userId;
    private String username;
    private String userProfileImageUrl;
    private String userCoverImageUrl;
    private boolean isFollowed;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUserProfileImageUrl() {
        return userProfileImageUrl;
    }

    public void setUserProfileImageUrl(String userProfileImageUrl) {
        this.userProfileImageUrl = userProfileImageUrl;
    }

    public String getUserCoverImageUrl() {
        return userCoverImageUrl;
    }

    public void setUserCoverImageUrl(String userCoverImageUrl) {
        this.userCoverImageUrl = userCoverImageUrl;
    }

    public boolean isFollowed() {
        return isFollowed;
    }

    public void setIsFollowed(boolean isFollowed) {
        this.isFollowed = isFollowed;
    }

    public User getUser(){
        if(!success)
            return null;
        User user = new User();
        user.setUsername(this.username);
        user.setUserId(this.userId);
        user.setIsFollowed(this.isFollowed);
        user.setUserCoverImageUrl(this.userCoverImageUrl);
        user.setUserProfileImageUrl(this.userProfileImageUrl);
        return user;
    };
}
