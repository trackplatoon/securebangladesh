package app.secure.com.myapplication.adapter;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import app.secure.com.myapplication.fragment.DiscoverPeopleFragment;
import app.secure.com.myapplication.fragment.NewsfeedFragment;

/**
 * Created by Saimon on 16-Oct-15.
 */
public class TabsPagerAdapter extends FragmentPagerAdapter {
    private NewsfeedFragment newsfeedFragment;
    private DiscoverPeopleFragment discoverPeopleFragment;

    public TabsPagerAdapter(FragmentManager fm, NewsfeedFragment newsfeedFragment, DiscoverPeopleFragment discoverPeopleFragment) {
        super(fm);
        this.newsfeedFragment = newsfeedFragment;
        this.discoverPeopleFragment = discoverPeopleFragment;
    }

    @Override
    public android.support.v4.app.Fragment getItem(int index) {

        switch (index) {
            case 0:
                return newsfeedFragment;
            case 1:
                return discoverPeopleFragment;
        }
        return null;
    }

    @Override
    public int getCount() {
        // get item count - equal to number of tabs
        return 2;
    }
}
