package app.secure.com.myapplication.pojo.requestprimitive;

import java.io.Serializable;

/**
 * Created by Saimon on 10-Jan-16.
 */
public class UserEntity implements Serializable {

    private String name;
    private String email;
    private String password;

    public UserEntity(String name,String email,String password){
        this.name = name;
        this.email =email;
        this.password = password;
    }

    public String getName() {
        return name;
    }



    public String getEmail() {
        return email;
    }



    public String getPassword() {
        return password;
    }


}
