package app.secure.com.myapplication.pojo.requestprimitive;

import java.util.List;

import app.secure.com.myapplication.pojo.User;

/**
 * Created by Saimon on 24-Mar-16.
 */
public class ResponseObjectDiscoverPeople {
    private boolean success;
    private List<User> users;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public List<User> getPeople() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }
}
