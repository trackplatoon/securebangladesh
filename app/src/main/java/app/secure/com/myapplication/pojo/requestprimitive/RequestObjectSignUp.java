package app.secure.com.myapplication.pojo.requestprimitive;

import com.fasterxml.jackson.annotation.JacksonAnnotation;
import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;

/**
 * Created by Saimon on 12-Jan-16.
 */
public class RequestObjectSignUp implements Serializable {
    private boolean success;
    private int userId;
    private String username;
    private String email;
    private String password;
    private String profileImage;
    private String coverImage;
    private String address;
    private String streetLine1;
    private String streetLine2;
    private String city;
    private String zipCode;
    private String region;
    private String country;
    private String birthday;
    private int sex;
    private String religion;
    private String mobileNumber;
    private String highSchool;
    private String college;
    private String university;
    private String profession;
    private String bloodGroup;


    public RequestObjectSignUp(){
        // do nothing.
    }

    // huge constructor
    public RequestObjectSignUp(String profileImage,
                               String coverImage,
                               String address,
                               String streetLine1,
                               String streetLine2,
                               String city,
                               String zipCode,
                               String region,
                               String country,
                               String birthday,
                               int sex,
                               String religion,
                               String mobileNumber,
                               String highSchool,
                               String college,
                               String university,
                               String profession,
                               String bloodGroup
    ) {
        this.profileImage = profileImage;
        this.coverImage = coverImage;
        this.address = address;
        this.streetLine1 = streetLine1;
        this.streetLine2 = streetLine2;
        this.city = city;
        this.zipCode = zipCode;
        this.region = region;
        this.country = country;
        this.birthday = birthday;
        this.sex = sex;
        this.religion = religion;
        this.mobileNumber = mobileNumber;
        this.highSchool = highSchool;
        this.college = college;
        this.university = university;
        this.profession = profession;
        this.bloodGroup = bloodGroup;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public String getCoverImage() {
        return coverImage;
    }

    public void setCoverImage(String coverImage) {
        this.coverImage = coverImage;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getStreetLine1() {
        return streetLine1;
    }

    public void setStreetLine1(String streetLine1) {
        this.streetLine1 = streetLine1;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public int getSex() {
        return sex;
    }

    public void setSex(int sex) {
        this.sex = sex;
    }

    public String getReligion() {
        return religion;
    }

    public void setReligion(String religion) {
        this.religion = religion;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getHighSchool() {
        return highSchool;
    }

    public void setHighSchool(String highSchool) {
        this.highSchool = highSchool;
    }

    public String getCollege() {
        return college;
    }

    public void setCollege(String college) {
        this.college = college;
    }

    public String getUniversity() {
        return university;
    }

    public void setUniversity(String university) {
        this.university = university;
    }

    public String getProfession() {
        return profession;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }

    public String getBloodGroup() {
        return bloodGroup;
    }

    public void setBloodGroup(String bloodGroup) {
        this.bloodGroup = bloodGroup;
    }

    public String getStreetLine2() {
        return streetLine2;
    }

    public void setStreetLine2(String streetLine2) {
        this.streetLine2 = streetLine2;
    }

    @JsonIgnore
    public String getSexAsString(){
        switch(sex){
            case 0:
                return "Male";
            case 1:
                return "Female";
            default:
                return "Other";
        }
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }
}
